import 'package:flutter/material.dart';
import 'dart:core';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:absensi_mobile/models/data.dart';
import 'package:objectdb/objectdb.dart';
import 'package:absensi_mobile/configs.dart' as configs;
import 'package:absensi_mobile/configs.dart';

class ProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProfilePage();
  }
}

class _ProfilePage extends State<ProfilePage> {
  ObjectDB db;
  String accessToken;
  User userData;
  TextEditingController firstname, phone, email, username, password;
  bool saving = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    configs.getDBPath().then((v) async {
      db = ObjectDB(v);

      created();
    });
  }

  void created() async {
    await db.open();
    List<Map> dbdata = await db.find({'type': 'logindata'});
    final adminData = dbdata.first['logindata'];
    String token = dbdata.first['accesstoken'];
    await db.close();

    if (adminData['user'] != null) {
      User tempUData = User.fromJson(adminData['user']);

      setState(() {
        password = TextEditingController(text: tempUData.password);
        firstname = TextEditingController(text: tempUData.firstname);
        phone = TextEditingController(text: tempUData.phone);
        email = TextEditingController(text: tempUData.email);
        username = TextEditingController(text: tempUData.username);
        this.userData = tempUData;
        this.accessToken = token;
      });
    }
  }

  void tryLogin() async {
    setState(() {
      saving = true;
    });
    LoginRequest req = LoginRequest();
    req.username = username.text;
    req.password = password.text;
    try {
      final response = await http.post(
        "${configs.serverUrl}/administrator/mlogin",
        body: req.toJson(),
      );
      if (response.statusCode == 200) {
        dynamic data = json.decode(response.body);
        UserLogin loginData = UserLogin.fromJson(data);
        await db.open();
        await db.remove({'type': 'logindata'});
        var ins = await db.insert({
          'logindata': loginData,
          'type': 'logindata',
          'accesstoken': loginData.token
        });

        List<Map> dbdata = await db.find({'type': 'logindata'});
        await db.close();
        showDialog(
            context: this.context,
            builder: (c) {
              return AlertDialog(
                title: Text("Data disimpan!"),
                content: Text("Data anda berhasil disimpan!"),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("Tutup"),
                  )
                ],
              );
            });
      } else {
        showDialog(
            builder: (c) {
              return AlertDialog(
                content: Text("Error in login! Wrong usename/password"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Close"),
                    onPressed: () {
                      Navigator.pop(c);
                    },
                  )
                ],
              );
            },
            context: this.context);
      }
    } catch (e) {
      print(e);
    } finally {
      setState(() {
        saving = false;
      });
    }
  }

  void save() async {
    setState(() {
      saving = true;
    });
    try {
      var resp = await http.put(
        "${configs.serverUrl}/enrollment/${userData.id.toString()}",
        body: {
          'firstname': firstname.text,
          'email': email.text,
          'phone': phone.text,
          'username': username.text,
          'password': password.text
        },
        headers: {'token': accessToken},
      );
      print(resp.body);
      if (resp.statusCode == 200) {
        tryLogin();
      }
    } catch (e) {
      print(e);
    } finally {
      setState(() {
        saving = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Informasi Profil"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {},
          ),
          FlatButton(
            child: Text(
              saving ? "Menyimpan..." : "Simpan",
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () {
              if (!saving) {
                save();
              }
            },
          )
        ],
      ),
      body: Builder(
        builder: (c) {
          if (this.userData != null)
            return profileForm();
          else
            return loader();
        },
      ),
    );
  }

  Widget formTextField(TextEditingController controller) => TextField(
        controller: controller,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(12, 8, 12, 8),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(1)),
                borderSide:
                    BorderSide(color: Colors.red, style: BorderStyle.none))),
        style: TextStyle(fontSize: 19),
      );

  Widget formLabel(String label) {
    return Container(
      padding: EdgeInsets.all(8),
      child: Text(
        label,
        style: TextStyle(fontSize: 13),
      ),
    );
  }

  Widget line() => Container(
        margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
        padding: EdgeInsets.fromLTRB(0, 1, 0, 0),
        color: Colors.grey[500],
      );
  Widget formTitle(IconData icon, String text) => Container(
        margin: EdgeInsets.fromLTRB(5, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
              child: Icon(
                icon,
                color: Colors.grey[500],
              ),
            ),
            Text(text,
                style: TextStyle(
                  fontSize: 16,
                ))
          ],
        ),
      );
  Widget profileForm() {
    return ListView(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              formTitle(Icons.info, "Personal Info"),
              formLabel("Nama Lengkap"),
              formTextField(firstname),
              formLabel("No Hp"),
              formTextField(phone),
              formLabel("Email"),
              formTextField(email),
              line(),
              formTitle(Icons.info, "Division Info"),
              formLabel("Divisi"),
              Container(
                padding: EdgeInsets.all(8),
                child: Text(
                  userData.division.division,
                  style: TextStyle(fontSize: 18),
                ),
              ),
              formLabel("Sta. Ketenagakerjaan"),
              Container(
                padding: EdgeInsets.all(8),
                child: Text(
                  userData.employmentStatus.employmentStatus,
                  style: TextStyle(fontSize: 18),
                ),
              ),
              line(),
              formTitle(Icons.vpn_key, "Login Information"),
              formLabel("Username"),
              formTextField(username),
              formLabel("Password"),
              formTextField(password),
            ],
          ),
        )
      ],
    );
  }

  Widget loader() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[CircularProgressIndicator(), Text("Loading...")],
      ),
    );
  }
}
