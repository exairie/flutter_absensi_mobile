import 'package:flutter/material.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:objectdb/objectdb.dart';

final String dbpath = ".aswaddb.db";
Future<String> getDBPath() async {
  final dir = await getApplicationDocumentsDirectory();
  return Future.value(dir.path + dbpath);
}

// final String serverUrl = "http://10.10.2.71:5000";
final String serverUrl = "http://datditdut.com:5000";
final String CHANNEL = "aswad.io/primary";
final String photoServer = "http://datditdut.com:5000/profile";
String getPhotoUrl(dynamic id) {
  return "$photoServer/${id.toString()}_photo1.jpg";
}

String toDateTimeString(int t) {
  int h = t ~/ 3600;
  t -= h * 3600;
  int m = t ~/ 60;
  t -= m * 60;

  return "${h}h ${m}m";
}

Future<void> transaction(
    ObjectDB db, Future<void> execution(ObjectDB db)) async {
  db.open().then((db) {
    execution(db).then((n) async {
      await db.close();
    });
  });
  return Future.value();
}

List<DateTime> createDateTime(String str) {
  var regex = RegExp("[0-9]{2}\\:[0-9]{2}\\s-\\s[0-9]{2}:[0-9]{2}");
  if (regex.hasMatch(str)) {
    var now = DateTime.now();
    var spstr = str.split("-").map((s) => s.trim()).toList();
    str = spstr[0];
    int h = int.parse(str.substring(0, 2));
    int m = int.parse(str.substring(3, 5));
    var n1 = DateTime(now.year, now.month, now.day, h, m, 0);
    str = spstr[1];
    h = int.parse(str.substring(0, 2));
    m = int.parse(str.substring(3, 5));
    var n2 = DateTime(now.year, now.month, now.day, h, m, 0);

    return [n1, n2];
  }
  return null;
}

DateTime createDateTimeSingle(String str) {
  var regex = RegExp("[0-9]{2}\\:[0-9]{2}");
  if (regex.hasMatch(str)) {
    var now = DateTime.now();
    int h = int.parse(str.substring(0, 2));
    int m = int.parse(str.substring(3, 5));
    var n1 = DateTime(now.year, now.month, now.day, h, m, 0);

    return n1;
  }
  return null;
}

DateTime createDateTimeFromHM(int h, int m) {
  var now = DateTime.now();
  return DateTime(now.year, now.month, now.day, h, m, 0);
}
