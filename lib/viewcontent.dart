import 'package:flutter/material.dart';

class ViewContent extends StatelessWidget{
  String text = "";
  ViewContent(String text){
    this.text = text;
  }
  @override
  Widget build(BuildContext context){
    return Container(
      decoration: new BoxDecoration(
        
      ),
      margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
      padding: EdgeInsets.all(4.0),
      child: Column(
        children : <Widget>[
          Row(        
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Column(  
                  crossAxisAlignment: CrossAxisAlignment.start,            
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(                  
                      padding: EdgeInsets.all(2.0),
                      child: Text("Ridwan Achadi Nugroho"),
                    ),
                    Padding(                                                    
                      padding: EdgeInsets.all(2.0),
                      child: Text("Ridwan Achadi Nugroho",
                        style: TextStyle(                      
                          fontSize: 12
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                width: 70.0,
                child: Text("06:59"),
              ),
              SizedBox(
                width: 60.0,
                child: Text("Telat"),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 1, 0, 0),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: Color.fromARGB(233, 233, 233, 233),width: 1.0)
              )
            ),
          )
        ])
    );
  }
}