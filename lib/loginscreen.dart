import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:absensi_mobile/configs.dart' as config;
import 'dart:convert';
import 'package:objectdb/objectdb.dart';
import 'package:absensi_mobile/models/data.dart';
import 'package:absensi_mobile/homepage.dart';
import 'package:absensi_mobile/adminpage.dart';
import 'package:flutter/services.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginScreen();
  }
}

class LoginScreen extends State<Login> {
  bool loginState = false;
  bool loading = false;
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  ObjectDB db;

  void created() async {
    db = ObjectDB(await config.getDBPath());
    db.open();

    checkUserInfo();
  }

  @override
  void initState() {
    super.initState();

    created();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    db.close();
  }

  void checkUserInfo() async {
    List<Map> dbdata = await db.find({'type': 'logindata'});
    try {
      if (dbdata.length < 1) return;
      final adminData = dbdata.first['logindata'];
      UserLogin loginData = UserLogin.fromJson(adminData);
      if (loginData.logintype == 'user') {
        if (adminData['user'] != null) {
          final mAdminData = User.fromJson(adminData['user']);
          if (mAdminData != null) {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (c) => HomePage()),
                ModalRoute.withName("./"));
          }
        }
      } else if (loginData.logintype == 'admin') {
        if (adminData['admin'] != null) {
          final mAdminData = User.fromJson(adminData['admin']);
          if (mAdminData != null) {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (c) => AdminPage()),
                ModalRoute.withName("./"));
          }
        }
      }
    } finally {
      await db.close();
    }
  }

  void tryLogin() async {
    setState(() {
      loading = true;
    });
    LoginRequest req = LoginRequest();
    req.username = username.text;
    req.password = password.text;
    try {
      final response = await http
          .post("${config.serverUrl}/administrator/mlogin", body: req.toJson());
      if (response.statusCode == 200) {
        dynamic data = json.decode(response.body);
        UserLogin loginData = UserLogin.fromJson(data);
        String j = json.encode(loginData);
        final db = ObjectDB(await config.getDBPath());
        print(response.body);
        await db.open();
        await db.remove({});
        await db.close();
        await db.open();
        await db.insert({
          'logindata': loginData,
          'type': 'logindata',
          'accesstoken': loginData.token
        });
        await db.close();
        print(loginData);
        if (loginData.logintype == 'user') {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (c) => HomePage()),
              ModalRoute.withName("./"));
        } else if (loginData.logintype == 'admin') {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (c) => AdminPage()),
              ModalRoute.withName("./"));
        }
      } else {
        showDialog(
            builder: (c) {
              return AlertDialog(
                content: Text("Error in login! Wrong usename/password"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Close"),
                    onPressed: () {
                      Navigator.pop(c);
                    },
                  )
                ],
              );
            },
            context: this.context);
      }
    } catch (e) {
      print(e);
      print("HALAA");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  Widget form() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
            padding: EdgeInsets.all(10),
            child: Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
              child: Center(
                child: Text(
                  "ASWAD Monitoring",
                  style: TextStyle(color: Colors.white, fontSize: 23),
                ),
              ),
            )),
        Center(
          child: Image(
            image: AssetImage("images/smk.png"),
            height: 80,
            width: 80,
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
          child: Center(
            child: Text(
              "Username",
              style: TextStyle(color: Colors.white, fontSize: 17),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(15, 15, 15, 0),
          child: TextField(
            controller: username,
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.verified_user),
              filled: true,
              fillColor: Colors.white,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Center(
            child: Text(
              "Password",
              style: TextStyle(color: Colors.white, fontSize: 17),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(15, 15, 15, 15),
          child: TextField(
            controller: password,
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.vpn_key),
              filled: true,
              fillColor: Colors.white,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(15, 0, 15, 15),
          child: Row(
            children: <Widget>[
              Expanded(
                  child: ButtonTheme(
                height: 50,
                child: RaisedButton(
                  shape: BeveledRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5.0))),
                  onPressed: () {
                    tryLogin();
                  },
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Login",
                        style: TextStyle(fontSize: 19),
                      ),
                      Builder(builder: (c) {
                        if (this.loading)
                          return CircularProgressIndicator();
                        else
                          return Icon(Icons.check_circle);
                      })
                    ],
                  ),
                  color: Colors.blueGrey,
                  textColor: Colors.white,
                ),
              ))
            ],
          ),
        )
      ],
    );
  }

  Widget bodyContent() {
    return Container(
        child: Center(
      child: SingleChildScrollView(
        child: form(),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 50, 59, 68), body: bodyContent());
  }
}
