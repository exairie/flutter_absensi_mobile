import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:absensi_mobile/models/data.dart';

class ListAbsensi extends StatefulWidget {
  List<User> attendance;
  ListAbsensi(this.attendance);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ListAbsensiState();
  }
}

class ListAbsensiState extends State<ListAbsensi> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Absensi Hari Ini"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {},
          )
        ],
      ),
      body: widgetBody(),
    );
  }

  Widget widgetBody() {
    return Container(
      padding: const EdgeInsets.all(8),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              children: <Widget>[
                Container(
                  child: Row(),
                ),
                AbsensiRow(
                  null,
                  isHeader: true,
                ),
                Expanded(
                  child: ListView.builder(
                    itemBuilder: (context, i) => AbsensiRow(
                          widget.attendance[i],
                          isHeader: false,
                        ),
                    itemCount: widget.attendance.length,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class AbsensiRow extends StatelessWidget {
  final User enrollment;
  bool isHeader;
  AbsensiRow(this.enrollment, {this.isHeader});
  Widget getStatus() {
    if (enrollment == null)
      return Text("-");
    else if (enrollment.permit != null) {
      return Container(
        color: Colors.amber,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Izin (${enrollment.permit.reason})",
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
    } else if (enrollment.attendance != null) {
      return Row(
        children: <Widget>[
          Icon(
            Icons.check,
            color: Colors.green,
          ),
          Text(
            "Sudah masuk",
            style: TextStyle(color: Colors.green),
          )
        ],
      );
    } else
      return Row(
        children: <Widget>[
          Icon(
            Icons.close,
            color: Colors.red,
          ),
          Text(
            "Belum masuk",
            style: TextStyle(color: Colors.red),
          )
        ],
      );
  }

  @override
  Widget build(BuildContext context) {
    double totalWidth = MediaQuery.of(context).size.width - 16;
    // TODO: implement build
    return Container(
      child: Row(
        children: <Widget>[
          SizedBox(
            width: .5 * totalWidth,
            child: Container(
              padding: EdgeInsets.all(8),
              child: Text(
                isHeader ? "Nama" : enrollment.firstname,
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontWeight: isHeader ? FontWeight.bold : FontWeight.normal),
              ),
            ),
          ),
          SizedBox(
            width: .5 * totalWidth,
            child: Container(
              padding: EdgeInsets.all(8),
              child: Builder(
                builder: (c) {
                  if (isHeader)
                    return Text(
                      "Status",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontWeight:
                              isHeader ? FontWeight.bold : FontWeight.normal),
                    );
                  return getStatus();
                },
              ),
            ),
          ),
          // SizedBox(
          //   width: .4 * totalWidth,
          //   child: Container(
          //     padding: EdgeInsets.all(8),
          //     child: Text(
          //       "Informasi",
          //       textAlign: TextAlign.center,
          //       style: TextStyle(
          //           fontWeight: isHeader ? FontWeight.bold : FontWeight.normal),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
