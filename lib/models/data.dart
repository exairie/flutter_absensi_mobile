import 'dart:core';
import 'package:absensi_mobile/configs.dart';

class LoginRequest {
  String username;
  String password;
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['password'] = this.password;

    return data;
  }
}

class PermitRequestData {
  int id;
  int enrollmentId;
  String permitType;
  String permitFrom;
  String permitTo;
  String reason;
  String status;
  String createdAt;
  String updatedAt;
  User enrollment;

  PermitRequestData(
      {this.id,
      this.enrollmentId,
      this.permitType,
      this.permitFrom,
      this.permitTo,
      this.reason,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.enrollment});

  PermitRequestData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    enrollmentId = json['enrollment_id'];
    permitType = json['permit_type'];
    permitFrom = json['permit_from'];
    permitTo = json['permit_to'];
    reason = json['reason'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    enrollment = json['enrollment'] != null
        ? new User.fromJson(json['enrollment'])
        : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['enrollment_id'] = this.enrollmentId;
    data['permit_type'] = this.permitType;
    data['permit_from'] = this.permitFrom;
    data['permit_to'] = this.permitTo;
    data['reason'] = this.reason;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.enrollment != null) {
      data['enrollment'] = this.enrollment.toJson();
    }
    return data;
  }
}

class UserLogin {
  String logintype;
  User user;
  String token;
  AdminData admin;

  UserLogin({this.logintype, this.user, this.token});

  UserLogin.fromJson(Map<String, dynamic> json) {
    logintype = json['logintype'];
    token = json['token'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    admin =
        json['admin'] != null ? new AdminData.fromJson(json['admin']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['logintype'] = this.logintype;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.admin != null) {
      data['admin'] = this.admin.toJson();
    }
    return data;
  }
}

class User {
  int id;
  String firstname;
  String lastname;
  String employmentStatusId;
  String divisionId;
  String nip;
  String nik;
  String gender;
  String phone;
  String email;
  String address;
  String grade;
  String fmd1;
  String fmd2;
  String fmd3;
  String username;
  String password;
  String createdAt;
  String updatedAt;
  String kota;
  String kecamatan;
  String kelurahan;
  String nuptk;
  String photo1;
  String photo2;
  int duk;
  int active;
  int adminId;
  String importance;
  String importanceDetail;
  Division division;
  EmploymentStatus employmentStatus;
  PersonalSchedule personalSchedule;
  List<EnrollmentSchedules> enrollmentSchedules;

  Attendance attendance;
  Permit permit;

  AdminData admin;

  User(
      {this.id,
      this.firstname,
      this.lastname,
      this.employmentStatusId,
      this.divisionId,
      this.nip,
      this.nik,
      this.gender,
      this.phone,
      this.email,
      this.address,
      this.grade,
      this.fmd1,
      this.fmd2,
      this.fmd3,
      this.username,
      this.password,
      this.createdAt,
      this.updatedAt,
      this.kota,
      this.kecamatan,
      this.kelurahan,
      this.nuptk,
      this.photo1,
      this.photo2,
      this.duk,
      this.active,
      this.adminId,
      this.division,
      this.employmentStatus,
      this.personalSchedule,
      this.enrollmentSchedules});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    employmentStatusId = json['employment_status_id'];
    divisionId = json['division_id'];
    nip = json['nip'];
    nik = json['nik'];
    gender = json['gender'];
    phone = json['phone'];
    email = json['email'];
    address = json['address'];
    grade = json['grade'];
    fmd1 = json['fmd1'];
    fmd2 = json['fmd2'];
    fmd3 = json['fmd3'];
    username = json['username'];
    password = json['password'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    kota = json['kota'];
    kecamatan = json['kecamatan'];
    kelurahan = json['kelurahan'];
    nuptk = json['nuptk'];
    photo1 = json['photo1'];
    photo2 = json['photo2'];
    duk = json['duk'];
    active = json['active'];
    adminId = json['admin_id'];
    importance = json['importance'];
    importanceDetail = json['importance_detail'];
    division = json['division'] != null
        ? new Division.fromJson(json['division'])
        : null;
    employmentStatus = json['employment_status'] != null
        ? new EmploymentStatus.fromJson(json['employment_status'])
        : null;
    personalSchedule = json['personal_schedule'] != null
        ? PersonalSchedule.fromJson(json['personal_schedule'])
        : null;
    if (json['enrollment_schedules'] != null) {
      enrollmentSchedules = new List<EnrollmentSchedules>();
      json['enrollment_schedules'].forEach((v) {
        enrollmentSchedules.add(new EnrollmentSchedules.fromJson(v));
      });
    }
    if (json['attendance'] != null) {
      this.attendance = Attendance.fromJson(json['attendance']);
    }
    if (json['permit'] != null) {
      this.permit = Permit.fromJson(json['permit']);
    }
    if (json['admin'] != null) this.admin = AdminData.fromJson(json['admin']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['employment_status_id'] = this.employmentStatusId;
    data['division_id'] = this.divisionId;
    data['nip'] = this.nip;
    data['nik'] = this.nik;
    data['gender'] = this.gender;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['address'] = this.address;
    data['grade'] = this.grade;
    data['fmd1'] = this.fmd1;
    data['fmd2'] = this.fmd2;
    data['fmd3'] = this.fmd3;
    data['username'] = this.username;
    data['password'] = this.password;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['kota'] = this.kota;
    data['kecamatan'] = this.kecamatan;
    data['kelurahan'] = this.kelurahan;
    data['nuptk'] = this.nuptk;
    data['photo1'] = this.photo1;
    data['photo2'] = this.photo2;
    data['duk'] = this.duk;
    data['active'] = this.active;
    data['admin_id'] = this.adminId;
    if (this.enrollmentSchedules != null) {
      data['enrollment_schedules'] =
          this.enrollmentSchedules.map((v) => v.toJson()).toList();
    }
    if (this.division != null) {
      data['division'] = this.division.toJson();
    }
    if (this.employmentStatus != null) {
      data['employment_status'] = this.employmentStatus.toJson();
    }
    data['personal_schedule'] = this.personalSchedule;
    if (this.enrollmentSchedules != null) {
      data['enrollment_schedules'] =
          this.enrollmentSchedules.map((v) => v.toJson()).toList();
    }
    if (this.admin != null) data['admin'] = this.admin.toJson();
    return data;
  }

  int getExpectedWorkHour() {
    if (division == null) return 0;
    if (personalSchedule == null) {
      return division == null ? 0 : division.getExpectedWorkHour() * 5;
    } else {
      int total = 0;
      if (personalSchedule.seninHadir == 1) {
        if (personalSchedule.seninCustom == 1) {
          var en = createDateTimeFromHM(personalSchedule.seninMasukJam,
                  personalSchedule.seninMasukMenit),
              ex = createDateTimeFromHM(personalSchedule.seninKeluarJam,
                  personalSchedule.seninKeluarMenit);
          total += ex.difference(en).inSeconds;
          if (division.isCrossingWorkHour(en, ex)) {
            total -= division.breakLength();
          }
        } else {
          total += division.getExpectedWorkHour();
        }
      }
      if (personalSchedule.selasaHadir == 1) {
        if (personalSchedule.selasaCustom == 1) {
          var en = createDateTimeFromHM(personalSchedule.selasaMasukJam,
                  personalSchedule.selasaMasukMenit),
              ex = createDateTimeFromHM(personalSchedule.selasaKeluarJam,
                  personalSchedule.selasaKeluarMenit);
          total += ex.difference(en).inSeconds;
          if (division.isCrossingWorkHour(en, ex)) {
            total -= division.breakLength();
          }
        } else {
          total += division.getExpectedWorkHour();
        }
      }
      if (personalSchedule.rabuHadir == 1) {
        if (personalSchedule.rabuCustom == 1) {
          var en = createDateTimeFromHM(personalSchedule.rabuMasukJam,
                  personalSchedule.rabuMasukMenit),
              ex = createDateTimeFromHM(personalSchedule.rabuKeluarJam,
                  personalSchedule.rabuKeluarMenit);
          total += ex.difference(en).inSeconds;
          if (division.isCrossingWorkHour(en, ex)) {
            total -= division.breakLength();
          }
        } else {
          total += division.getExpectedWorkHour();
        }
      }
      if (personalSchedule.kamisHadir == 1) {
        if (personalSchedule.kamisCustom == 1) {
          var en = createDateTimeFromHM(personalSchedule.kamisMasukJam,
                  personalSchedule.kamisMasukMenit),
              ex = createDateTimeFromHM(personalSchedule.kamisKeluarJam,
                  personalSchedule.kamisKeluarMenit);
          total += ex.difference(en).inSeconds;
          if (division.isCrossingWorkHour(en, ex)) {
            total -= division.breakLength();
          }
        } else {
          total += division.getExpectedWorkHour();
        }
      }
      if (personalSchedule.jumatHadir == 1) {
        if (personalSchedule.jumatCustom == 1) {
          var en = createDateTimeFromHM(personalSchedule.jumatMasukJam,
                  personalSchedule.jumatMasukMenit),
              ex = createDateTimeFromHM(personalSchedule.jumatKeluarJam,
                  personalSchedule.jumatKeluarMenit);
          total += ex.difference(en).inSeconds;
          if (division.isCrossingWorkHour(en, ex)) {
            total -= division.breakLength();
          }
        } else {
          total += division.getExpectedWorkHour();
        }
      }

      return total;
    }
  }
}

class HourDefinition {
  int id;
  int hour;
  String senin;
  String selasa;
  String rabu;
  String kamis;
  String jumat;
  DateTime createdAt;
  DateTime updatedAt;

  HourDefinition(
      {this.id,
      this.hour,
      this.senin,
      this.selasa,
      this.rabu,
      this.kamis,
      this.jumat,
      this.createdAt,
      this.updatedAt});

  HourDefinition.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    hour = json['hour'];
    senin = json['senin'];
    selasa = json['selasa'];
    rabu = json['rabu'];
    kamis = json['kamis'];
    jumat = json['jumat'];
    createdAt = DateTime.parse(json['created_at']).toLocal();
    updatedAt = DateTime.parse(json['updated_at']).toLocal();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['hour'] = this.hour;
    data['senin'] = this.senin;
    data['selasa'] = this.selasa;
    data['rabu'] = this.rabu;
    data['kamis'] = this.kamis;
    data['jumat'] = this.jumat;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class EnrollmentSchedules {
  int id;
  int enrollmentId;
  int hour;
  String day;
  String schedule;
  String assignedClass;
  DateTime createdAt;
  DateTime updatedAt;
  HourDefinition hourDefinition;

  EnrollmentSchedules(
      {this.id,
      this.enrollmentId,
      this.hour,
      this.day,
      this.schedule,
      this.assignedClass,
      this.createdAt,
      this.updatedAt,
      this.hourDefinition});

  EnrollmentSchedules.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    enrollmentId = json['enrollment_id'];
    hour = json['hour'];
    day = json['day'];
    schedule = json['schedule'];
    assignedClass = json['class'];
    createdAt = DateTime.parse(json['created_at']).toLocal();
    updatedAt = DateTime.parse(json['updated_at']).toLocal();
    hourDefinition = json['hour_definition'] != null
        ? new HourDefinition.fromJson(json['hour_definition'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['enrollment_id'] = this.enrollmentId;
    data['hour'] = this.hour;
    data['day'] = this.day;
    data['schedule'] = this.schedule;
    data['class'] = this.assignedClass;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.hourDefinition != null) {
      data['hour_definition'] = this.hourDefinition.toJson();
    }
    return data;
  }
}

class AdminLogin {
  String token;
  AdminData data;

  AdminLogin({this.token, this.data});

  AdminLogin.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    data = json['data'] != null ? new AdminData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class AdminData {
  int id;
  String username;
  String password;
  int level;
  String name;
  String email;
  String phone;
  Null createdAt;
  Null updatedAt;

  AdminData(
      {this.id,
      this.username,
      this.password,
      this.level,
      this.name,
      this.email,
      this.phone,
      this.createdAt,
      this.updatedAt});

  AdminData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    password = json['password'];
    level = json['level'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['password'] = this.password;
    data['level'] = this.level;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Absensi {
  int id;
  String firstname;
  String lastname;
  String employmentStatusId;
  String divisionId;
  String nip;
  String nik;
  String gender;
  String phone;
  String email;
  String address;
  String grade;
  String fmd1;
  String fmd2;
  String fmd3;
  String username;
  String password;
  String createdAt;
  String updatedAt;
  String kota;
  String kecamatan;
  String kelurahan;
  String nuptk;
  Division division;
  EmploymentStatus employmentStatus;
  Attendance attendance;

  Absensi(
      {this.id,
      this.firstname,
      this.lastname,
      this.employmentStatusId,
      this.divisionId,
      this.nip,
      this.nik,
      this.gender,
      this.phone,
      this.email,
      this.address,
      this.grade,
      this.fmd1,
      this.fmd2,
      this.fmd3,
      this.username,
      this.password,
      this.createdAt,
      this.updatedAt,
      this.kota,
      this.kecamatan,
      this.kelurahan,
      this.nuptk,
      this.division,
      this.employmentStatus,
      this.attendance});

  Absensi.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    employmentStatusId = json['employment_status_id'];
    divisionId = json['division_id'];
    nip = json['nip'];
    nik = json['nik'];
    gender = json['gender'];
    phone = json['phone'];
    email = json['email'];
    address = json['address'];
    grade = json['grade'];
    fmd1 = json['fmd1'];
    fmd2 = json['fmd2'];
    fmd3 = json['fmd3'];
    username = json['username'];
    password = json['password'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    kota = json['kota'];
    kecamatan = json['kecamatan'];
    kelurahan = json['kelurahan'];
    nuptk = json['nuptk'];
    division = json['division'] != null
        ? new Division.fromJson(json['division'])
        : null;
    employmentStatus = json['employment_status'] != null
        ? new EmploymentStatus.fromJson(json['employment_status'])
        : null;
    attendance = json['attendance'] != null
        ? new Attendance.fromJson(json['attendance'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['employment_status_id'] = this.employmentStatusId;
    data['division_id'] = this.divisionId;
    data['nip'] = this.nip;
    data['nik'] = this.nik;
    data['gender'] = this.gender;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['address'] = this.address;
    data['grade'] = this.grade;
    data['fmd1'] = this.fmd1;
    data['fmd2'] = this.fmd2;
    data['fmd3'] = this.fmd3;
    data['username'] = this.username;
    data['password'] = this.password;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['kota'] = this.kota;
    data['kecamatan'] = this.kecamatan;
    data['kelurahan'] = this.kelurahan;
    data['nuptk'] = this.nuptk;
    if (this.division != null) {
      data['division'] = this.division.toJson();
    }
    if (this.employmentStatus != null) {
      data['employment_status'] = this.employmentStatus.toJson();
    }
    if (this.attendance != null) {
      data['attendance'] = this.attendance.toJson();
    }
    return data;
  }
}

class Division {
  String id;
  int shiftStartHour;
  int shiftEndHour;
  int shiftStartMinute;
  int shiftEndMinute;
  String division;
  String createdAt;
  String updatedAt;
  int breakHour;
  int breakMinute;
  int breakHourEnd;
  int breakMinuteEnd;
  int closingHour;
  int closingMinute;
  int estimatedWorkHour;

  DateTime breakStart() {
    return createDateTimeFromHM(breakHour, breakMinute);
  }

  DateTime breakEnd() {
    return createDateTimeFromHM(breakHourEnd, breakMinuteEnd);
  }

  DateTime entry() {
    return createDateTimeFromHM(shiftStartHour, shiftStartMinute);
  }

  DateTime exit() {
    return createDateTimeFromHM(shiftEndHour, shiftEndMinute);
  }

  int breakLength() {
    return breakEnd().difference(breakStart()).inSeconds;
  }

  int getExpectedWorkHour() {
    var hwork = exit().difference(entry()).inSeconds;
    var hbreak = breakEnd().difference(breakStart()).inSeconds;
    return hwork - hbreak;
  }

  bool isCrossingWorkHour(DateTime from, DateTime to) {
    return (from.millisecondsSinceEpoch < breakStart().millisecondsSinceEpoch &&
        to.millisecondsSinceEpoch > breakEnd().millisecondsSinceEpoch);
  }

  Division(
      {this.id,
      this.shiftStartHour,
      this.shiftEndHour,
      this.shiftStartMinute,
      this.shiftEndMinute,
      this.division,
      this.createdAt,
      this.updatedAt,
      this.breakHour,
      this.breakMinute,
      this.breakHourEnd,
      this.breakMinuteEnd,
      this.closingHour,
      this.closingMinute,
      this.estimatedWorkHour});

  Division.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    shiftStartHour = json['shift_start_hour'];
    shiftEndHour = json['shift_end_hour'];
    shiftStartMinute = json['shift_start_minute'];
    shiftEndMinute = json['shift_end_minute'];
    division = json['division'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    breakHour = json['break_hour'];
    breakMinute = json['break_minute'];
    breakHourEnd = json['break_hour_end'];
    breakMinuteEnd = json['break_minute_end'];
    closingHour = json['closing_hour'];
    closingMinute = json['closing_minute'];
    estimatedWorkHour = json['estimated_work_hour'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['shift_start_hour'] = this.shiftStartHour;
    data['shift_end_hour'] = this.shiftEndHour;
    data['shift_start_minute'] = this.shiftStartMinute;
    data['shift_end_minute'] = this.shiftEndMinute;
    data['division'] = this.division;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['break_hour'] = this.breakHour;
    data['break_minute'] = this.breakMinute;
    data['break_hour_end'] = this.breakHourEnd;
    data['break_minute_end'] = this.breakMinuteEnd;
    data['closing_hour'] = this.closingHour;
    data['closing_minute'] = this.closingMinute;
    data['estimated_work_hour'] = this.estimatedWorkHour;
    return data;
  }
}

class EmploymentStatus {
  String id;
  String employmentStatus;
  String createdAt;
  String updatedAt;

  EmploymentStatus(
      {this.id, this.employmentStatus, this.createdAt, this.updatedAt});

  EmploymentStatus.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    employmentStatus = json['employment_status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['employment_status'] = this.employmentStatus;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Attendance {
  int id;
  int enrollmentId;
  int shiftStartHour;
  int shiftEndHour;
  int shiftStartMinute;
  int shiftEndMinute;
  DateTime entryDate;
  String fmd;
  int entryStatus;
  DateTime exitDate;
  String autoDisposalDate;
  String exitFmd;
  String exitWorkHour;
  int entryDelta;
  int exitDelta;
  int totalWorkSec;
  int closingAuto;
  int entryAuto;
  String closingReason;
  String entryAutoReason;
  DateTime createdAt;
  DateTime updatedAt;
  Enrollment enrollment;

  Attendance(
      {this.id,
      this.enrollmentId,
      this.shiftStartHour,
      this.shiftEndHour,
      this.shiftStartMinute,
      this.shiftEndMinute,
      this.entryDate,
      this.fmd,
      this.entryStatus,
      this.exitDate,
      this.autoDisposalDate,
      this.exitFmd,
      this.exitWorkHour,
      this.entryDelta,
      this.exitDelta,
      this.totalWorkSec,
      this.closingAuto,
      this.entryAuto,
      this.closingReason,
      this.entryAutoReason,
      this.createdAt,
      this.updatedAt,
      this.enrollment});

  Attendance.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    enrollmentId = json['enrollment_id'];
    shiftStartHour = json['shift_start_hour'];
    shiftEndHour = json['shift_end_hour'];
    shiftStartMinute = json['shift_start_minute'];
    shiftEndMinute = json['shift_end_minute'];
    entryDate = DateTime.parse(json['entry_date']).toLocal();
    fmd = json['fmd'];
    entryStatus = json['entry_status'];
    if (json['exit_date'] != null)
      exitDate = DateTime.parse(json['exit_date']).toLocal();
    autoDisposalDate = json['auto_disposal_date'];
    exitFmd = json['exit_fmd'];
    exitWorkHour = json['exit_work_hour'].toString();
    entryDelta = json['entry_delta'];
    exitDelta = json['exit_delta'];
    totalWorkSec = json['total_work_sec'];
    closingAuto = json['closing_auto'];
    entryAuto = json['entry_auto'];
    closingReason = json['closing_reason'];
    entryAutoReason = json['entry_auto_reason'];
    createdAt = DateTime.parse(json['created_at']).toLocal();
    updatedAt = DateTime.parse(json['updated_at']).toLocal();
    enrollment = json['enrollment'] != null
        ? new Enrollment.fromJson(json['enrollment'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['enrollment_id'] = this.enrollmentId;
    data['shift_start_hour'] = this.shiftStartHour;
    data['shift_end_hour'] = this.shiftEndHour;
    data['shift_start_minute'] = this.shiftStartMinute;
    data['shift_end_minute'] = this.shiftEndMinute;
    data['entry_date'] = this.entryDate;
    data['fmd'] = this.fmd;
    data['entry_status'] = this.entryStatus;
    data['exit_date'] = this.exitDate;
    data['auto_disposal_date'] = this.autoDisposalDate;
    data['exit_fmd'] = this.exitFmd;
    data['exit_work_hour'] = this.exitWorkHour;
    data['entry_delta'] = this.entryDelta;
    data['exit_delta'] = this.exitDelta;
    data['total_work_sec'] = this.totalWorkSec;
    data['closing_auto'] = this.closingAuto;
    data['entry_auto'] = this.entryAuto;
    data['closing_reason'] = this.closingReason;
    data['entry_auto_reason'] = this.entryAutoReason;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.enrollment != null) {
      data['enrollment'] = this.enrollment.toJson();
    }
    return data;
  }

  bool isCrossingBreakTime(Division d) {
    if (exitDate == null) return false;
    return entryDate.millisecondsSinceEpoch <
            d.breakStart().millisecondsSinceEpoch &&
        exitDate.millisecondsSinceEpoch > d.breakEnd().millisecondsSinceEpoch;
  }

  int totalWorkTime() {
    if (exitDate == null) return 0;

    return exitDate.difference(entryDate).inSeconds;
  }

  int getRealWorkTime(Division d) {
    int workTime = totalWorkTime();
    if (isCrossingBreakTime(d))
      workTime -= d.breakEnd().difference(d.breakStart()).inSeconds;

    return workTime;
  }
}

class Enrollment {
  int id;
  String firstname;
  String lastname;
  String employmentStatusId;
  String divisionId;
  String nip;
  String nik;
  String gender;
  String phone;
  String email;
  String address;
  String grade;
  String fmd1;
  String fmd2;
  String fmd3;
  String username;
  String password;
  String createdAt;
  String updatedAt;
  String kota;
  String kecamatan;
  String kelurahan;
  String nuptk;
  String photo1;
  String photo2;
  int duk;
  int active;
  Null adminId;
  List<EnrollmentSchedules> enrollmentSchedules;

  PersonalSchedule personalSchedule;
  Division division;
  EmploymentStatus employmentStatus;
  List<Attendance> attendances;

  Enrollment(
      {this.id,
      this.firstname,
      this.lastname,
      this.employmentStatusId,
      this.divisionId,
      this.nip,
      this.nik,
      this.gender,
      this.phone,
      this.email,
      this.address,
      this.grade,
      this.fmd1,
      this.fmd2,
      this.fmd3,
      this.username,
      this.password,
      this.createdAt,
      this.updatedAt,
      this.kota,
      this.kecamatan,
      this.kelurahan,
      this.nuptk,
      this.photo1,
      this.photo2,
      this.duk,
      this.active,
      this.adminId,
      this.enrollmentSchedules,
      this.personalSchedule,
      this.division,
      this.employmentStatus,
      this.attendances});

  Enrollment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    employmentStatusId = json['employment_status_id'];
    divisionId = json['division_id'];
    nip = json['nip'];
    nik = json['nik'];
    gender = json['gender'];
    phone = json['phone'];
    email = json['email'];
    address = json['address'];
    grade = json['grade'];
    fmd1 = json['fmd1'];
    fmd2 = json['fmd2'];
    fmd3 = json['fmd3'];
    username = json['username'];
    password = json['password'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    kota = json['kota'];
    kecamatan = json['kecamatan'];
    kelurahan = json['kelurahan'];
    nuptk = json['nuptk'];
    photo1 = json['photo1'];
    photo2 = json['photo2'];
    duk = json['duk'];
    active = json['active'];
    adminId = json['admin_id'];
    personalSchedule = json['personal_schedule'] != null
        ? PersonalSchedule.fromJson(json['personal_schedule'])
        : null;
    if (json['enrollment_schedules'] != null) {
      enrollmentSchedules = new List<EnrollmentSchedules>();
      json['enrollment_schedules'].forEach((v) {
        enrollmentSchedules.add(new EnrollmentSchedules.fromJson(v));
      });
    }
    division = json['division'] != null
        ? new Division.fromJson(json['division'])
        : null;
    employmentStatus = json['employment_status'] != null
        ? new EmploymentStatus.fromJson(json['employment_status'])
        : null;
    if (json['attendances'] != null) {
      attendances = new List<Null>();
      json['attendances'].forEach((v) {
        attendances.add(new Attendance.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['employment_status_id'] = this.employmentStatusId;
    data['division_id'] = this.divisionId;
    data['nip'] = this.nip;
    data['nik'] = this.nik;
    data['gender'] = this.gender;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['address'] = this.address;
    data['grade'] = this.grade;
    data['fmd1'] = this.fmd1;
    data['fmd2'] = this.fmd2;
    data['fmd3'] = this.fmd3;
    data['username'] = this.username;
    data['password'] = this.password;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['kota'] = this.kota;
    data['kecamatan'] = this.kecamatan;
    data['kelurahan'] = this.kelurahan;
    data['nuptk'] = this.nuptk;
    data['photo1'] = this.photo1;
    data['photo2'] = this.photo2;
    data['duk'] = this.duk;
    data['active'] = this.active;
    data['admin_id'] = this.adminId;
    if (this.enrollmentSchedules != null) {
      data['enrollment_schedules'] =
          this.enrollmentSchedules.map((v) => v.toJson()).toList();
    }
    data['personal_schedule'] = this.personalSchedule;
    if (this.division != null) {
      data['division'] = this.division.toJson();
    }
    if (this.personalSchedule != null) {
      data['personal_schedule'] = this.personalSchedule.toJson();
    }
    if (this.employmentStatus != null) {
      data['employment_status'] = this.employmentStatus.toJson();
    }
    if (this.attendances != null) {
      data['attendances'] = this.attendances.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AttendanceLogs {
  int id;
  int enrollmentId;
  int shiftStartHour;
  int shiftEndHour;
  int shiftStartMinute;
  int shiftEndMinute;
  DateTime entryDate;
  String fmd;
  int entryStatus;
  Enrollment enrollment;
  DateTime exitDate;
  DateTime autoDisposalDate;
  String exitFmd;
  int exitWorkHour;
  int entryDelta;
  int exitDelta;
  int totalWorkSec;
  int closingAuto;
  int entryAuto;
  String closingReason;
  String entryAutoReason;
  String position;
  int offday;
  Permit permit;
  String createdAt;
  String updatedAt;

  AttendanceLogs(
      {this.id,
      this.enrollmentId,
      this.shiftStartHour,
      this.shiftEndHour,
      this.shiftStartMinute,
      this.shiftEndMinute,
      this.entryDate,
      this.fmd,
      this.entryStatus,
      this.exitDate,
      this.autoDisposalDate,
      this.exitFmd,
      this.exitWorkHour,
      this.entryDelta,
      this.exitDelta,
      this.totalWorkSec,
      this.closingAuto,
      this.entryAuto,
      this.closingReason,
      this.entryAutoReason,
      this.position,
      this.offday,
      this.createdAt,
      this.updatedAt});

  AttendanceLogs.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    enrollmentId = json['enrollment_id'];
    shiftStartHour = json['shift_start_hour'];
    shiftEndHour = json['shift_end_hour'];
    shiftStartMinute = json['shift_start_minute'];
    shiftEndMinute = json['shift_end_minute'];
    entryDate = DateTime.parse(json['entry_date']).toLocal();
    fmd = json['fmd'];
    entryStatus = json['entry_status'];
    if (json['exit_date'] != null)
      exitDate = DateTime.parse(json['exit_date']).toLocal();
    if (json['auto_disposal_date'] != null)
      autoDisposalDate = DateTime.parse(json['auto_disposal_date']).toLocal();
    exitFmd = json['exit_fmd'];
    exitWorkHour = json['exit_work_hour'];
    entryDelta = json['entry_delta'];
    exitDelta = json['exit_delta'];
    totalWorkSec = json['total_work_sec'];
    closingAuto = json['closing_auto'];
    entryAuto = json['entry_auto'];
    closingReason = json['closing_reason'];
    entryAutoReason = json['entry_auto_reason'];
    position = json['position'];
    offday = json['offday'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['enrollment'] != null)
      this.enrollment = Enrollment.fromJson(json['enrollment']);
    if (json['permit'] != null) this.permit = Permit.fromJson(json['permit']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['enrollment_id'] = this.enrollmentId;
    data['shift_start_hour'] = this.shiftStartHour;
    data['shift_end_hour'] = this.shiftEndHour;
    data['shift_start_minute'] = this.shiftStartMinute;
    data['shift_end_minute'] = this.shiftEndMinute;
    data['entry_date'] = this.entryDate;
    data['fmd'] = this.fmd;
    data['entry_status'] = this.entryStatus;
    data['exit_date'] = this.exitDate;
    data['auto_disposal_date'] = this.autoDisposalDate;
    data['exit_fmd'] = this.exitFmd;
    data['exit_work_hour'] = this.exitWorkHour;
    data['entry_delta'] = this.entryDelta;
    data['exit_delta'] = this.exitDelta;
    data['total_work_sec'] = this.totalWorkSec;
    data['closing_auto'] = this.closingAuto;
    data['entry_auto'] = this.entryAuto;
    data['closing_reason'] = this.closingReason;
    data['entry_auto_reason'] = this.entryAutoReason;
    data['position'] = this.position;
    data['offday'] = this.offday;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }

  bool isCrossingBreakTime(Division d) {
    if (exitDate == null) return false;
    return entryDate.millisecondsSinceEpoch <
            d.breakStart().millisecondsSinceEpoch &&
        exitDate.millisecondsSinceEpoch > d.breakEnd().millisecondsSinceEpoch;
  }

  int totalWorkTime() {
    if (exitDate == null) return 0;

    return exitDate.difference(entryDate).inSeconds;
  }

  int getRealWorkTime(Division d) {
    int workTime = totalWorkTime();
    if (isCrossingBreakTime(d))
      workTime -= d.breakEnd().difference(d.breakStart()).inSeconds;

    return workTime;
  }
}

class PersonalSchedule {
  int id;
  int enrollmentId;
  int seninHadir;
  int seninCustom;
  int seninMasukJam;
  int seninMasukMenit;
  int seninKeluarJam;
  int seninKeluarMenit;
  int selasaHadir;
  int selasaCustom;
  int selasaMasukJam;
  int selasaMasukMenit;
  int selasaKeluarJam;
  int selasaKeluarMenit;
  int rabuHadir;
  int rabuCustom;
  int rabuMasukJam;
  int rabuMasukMenit;
  int rabuKeluarJam;
  int rabuKeluarMenit;
  int kamisHadir;
  int kamisCustom;
  int kamisMasukJam;
  int kamisMasukMenit;
  int kamisKeluarJam;
  int kamisKeluarMenit;
  int jumatHadir;
  int jumatCustom;
  int jumatMasukJam;
  int jumatMasukMenit;
  int jumatKeluarJam;
  int jumatKeluarMenit;
  String createdAt;
  String updatedAt;

  PersonalSchedule(
      {this.id,
      this.enrollmentId,
      this.seninHadir,
      this.seninCustom,
      this.seninMasukJam,
      this.seninMasukMenit,
      this.seninKeluarJam,
      this.seninKeluarMenit,
      this.selasaHadir,
      this.selasaCustom,
      this.selasaMasukJam,
      this.selasaMasukMenit,
      this.selasaKeluarJam,
      this.selasaKeluarMenit,
      this.rabuHadir,
      this.rabuCustom,
      this.rabuMasukJam,
      this.rabuMasukMenit,
      this.rabuKeluarJam,
      this.rabuKeluarMenit,
      this.kamisHadir,
      this.kamisCustom,
      this.kamisMasukJam,
      this.kamisMasukMenit,
      this.kamisKeluarJam,
      this.kamisKeluarMenit,
      this.jumatHadir,
      this.jumatCustom,
      this.jumatMasukJam,
      this.jumatMasukMenit,
      this.jumatKeluarJam,
      this.jumatKeluarMenit,
      this.createdAt,
      this.updatedAt});

  PersonalSchedule.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    enrollmentId = json['enrollment_id'];
    seninHadir = json['senin_hadir'];
    seninCustom = json['senin_custom'];
    seninMasukJam = json['senin_masuk_jam'];
    seninMasukMenit = json['senin_masuk_menit'];
    seninKeluarJam = json['senin_keluar_jam'];
    seninKeluarMenit = json['senin_keluar_menit'];
    selasaHadir = json['selasa_hadir'];
    selasaCustom = json['selasa_custom'];
    selasaMasukJam = json['selasa_masuk_jam'];
    selasaMasukMenit = json['selasa_masuk_menit'];
    selasaKeluarJam = json['selasa_keluar_jam'];
    selasaKeluarMenit = json['selasa_keluar_menit'];
    rabuHadir = json['rabu_hadir'];
    rabuCustom = json['rabu_custom'];
    rabuMasukJam = json['rabu_masuk_jam'];
    rabuMasukMenit = json['rabu_masuk_menit'];
    rabuKeluarJam = json['rabu_keluar_jam'];
    rabuKeluarMenit = json['rabu_keluar_menit'];
    kamisHadir = json['kamis_hadir'];
    kamisCustom = json['kamis_custom'];
    kamisMasukJam = json['kamis_masuk_jam'];
    kamisMasukMenit = json['kamis_masuk_menit'];
    kamisKeluarJam = json['kamis_keluar_jam'];
    kamisKeluarMenit = json['kamis_keluar_menit'];
    jumatHadir = json['jumat_hadir'];
    jumatCustom = json['jumat_custom'];
    jumatMasukJam = json['jumat_masuk_jam'];
    jumatMasukMenit = json['jumat_masuk_menit'];
    jumatKeluarJam = json['jumat_keluar_jam'];
    jumatKeluarMenit = json['jumat_keluar_menit'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['enrollment_id'] = this.enrollmentId;
    data['senin_hadir'] = this.seninHadir;
    data['senin_custom'] = this.seninCustom;
    data['senin_masuk_jam'] = this.seninMasukJam;
    data['senin_masuk_menit'] = this.seninMasukMenit;
    data['senin_keluar_jam'] = this.seninKeluarJam;
    data['senin_keluar_menit'] = this.seninKeluarMenit;
    data['selasa_hadir'] = this.selasaHadir;
    data['selasa_custom'] = this.selasaCustom;
    data['selasa_masuk_jam'] = this.selasaMasukJam;
    data['selasa_masuk_menit'] = this.selasaMasukMenit;
    data['selasa_keluar_jam'] = this.selasaKeluarJam;
    data['selasa_keluar_menit'] = this.selasaKeluarMenit;
    data['rabu_hadir'] = this.rabuHadir;
    data['rabu_custom'] = this.rabuCustom;
    data['rabu_masuk_jam'] = this.rabuMasukJam;
    data['rabu_masuk_menit'] = this.rabuMasukMenit;
    data['rabu_keluar_jam'] = this.rabuKeluarJam;
    data['rabu_keluar_menit'] = this.rabuKeluarMenit;
    data['kamis_hadir'] = this.kamisHadir;
    data['kamis_custom'] = this.kamisCustom;
    data['kamis_masuk_jam'] = this.kamisMasukJam;
    data['kamis_masuk_menit'] = this.kamisMasukMenit;
    data['kamis_keluar_jam'] = this.kamisKeluarJam;
    data['kamis_keluar_menit'] = this.kamisKeluarMenit;
    data['jumat_hadir'] = this.jumatHadir;
    data['jumat_custom'] = this.jumatCustom;
    data['jumat_masuk_jam'] = this.jumatMasukJam;
    data['jumat_masuk_menit'] = this.jumatMasukMenit;
    data['jumat_keluar_jam'] = this.jumatKeluarJam;
    data['jumat_keluar_menit'] = this.jumatKeluarMenit;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Permit {
  int id;
  int enrollmentId;
  DateTime permitFrom;
  DateTime permitTo;
  int entryHour;
  int entryMinute;
  int bypassAttendance;
  int exitHour;
  int exitMinute;
  String reason;
  String status;
  String permitType;
  String permitBy;
  String createdAt;
  String updatedAt;

  Permit(
      {this.id,
      this.enrollmentId,
      this.permitFrom,
      this.permitTo,
      this.entryHour,
      this.entryMinute,
      this.bypassAttendance,
      this.exitHour,
      this.exitMinute,
      this.reason,
      this.status,
      this.permitType,
      this.permitBy,
      this.createdAt,
      this.updatedAt});

  Permit.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    enrollmentId = json['enrollment_id'];
    permitFrom = DateTime.parse(json['permit_from']).toLocal();
    permitTo = DateTime.parse(json['permit_to']).toLocal();
    entryHour = json['entry_hour'];
    entryMinute = json['entry_minute'];
    bypassAttendance = json['bypass_attendance'];
    exitHour = json['exit_hour'];
    exitMinute = json['exit_minute'];
    reason = json['reason'];
    status = json['status'];
    permitType = json['permit_type'];
    permitBy = json['permit_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['enrollment_id'] = this.enrollmentId;
    data['permit_from'] = this.permitFrom;
    data['permit_to'] = this.permitTo;
    data['entry_hour'] = this.entryHour;
    data['entry_minute'] = this.entryMinute;
    data['bypass_attendance'] = this.bypassAttendance;
    data['exit_hour'] = this.exitHour;
    data['exit_minute'] = this.exitMinute;
    data['reason'] = this.reason;
    data['status'] = this.status;
    data['permit_type'] = this.permitType;
    data['permit_by'] = this.permitBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
