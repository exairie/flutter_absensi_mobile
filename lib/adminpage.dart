import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:objectdb/objectdb.dart';
import 'package:absensi_mobile/models/data.dart';
import 'package:absensi_mobile/configs.dart' as configs;
import 'package:absensi_mobile/loginscreen.dart';
import 'package:absensi_mobile/homepage.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:absensi_mobile/listabsensi.dart';
import 'package:absensi_mobile/permitrequest.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:absensi_mobile/profileinfo.dart';

class AdminPage extends StatefulWidget {
  AdminData adminData;
  AdminPage({this.adminData, this.token});
  String token;
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AdminPage(adminData: adminData, token: this.token);
  }
}

class DashboardSumData {
  int sudahMasuk = 0,
      belumMasuk = 0,
      izin = 0,
      izinLain = 0,
      cuti = 0,
      cutiLain = 0,
      sakit = 0;
  DashboardSumData(
      {this.sudahMasuk = 0,
      this.sakit = 0,
      this.belumMasuk = 0,
      this.izin = 0,
      this.izinLain = 0,
      this.cuti = 0,
      this.cutiLain = 0});
}

class _AdminPage extends State<AdminPage> with SingleTickerProviderStateMixin {
  TextEditingController searchTextController = TextEditingController();
  bool isSearching = false;
  AnimationController searchAnimationController;
  _AdminPage({this.adminData, this.token});
  List<User> data = List<User>();
  DateTime now = DateTime.now();
  Timer timer, timerLoader;
  ObjectDB db;
  AdminData adminData;
  String token;
  DashboardSumData dataGuru = DashboardSumData();
  DashboardSumData dataTU = DashboardSumData();

  final GlobalKey<AnimatedCircularChartState> chartGuru =
      GlobalKey<AnimatedCircularChartState>();
  final GlobalKey<AnimatedCircularChartState> chartTU =
      GlobalKey<AnimatedCircularChartState>();
  final GlobalKey<SearchBarState> searchState = GlobalKey<SearchBarState>();
  bool loading = false;
  @override
  void didUpdateWidget(AdminPage oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  void loadData() async {
    setState(() {
      loading = true;
    });
    try {
      var resp = await http.get("${configs.serverUrl}/attendance_log/log");
      if (resp.statusCode == 200) {
        dynamic data = json.decode(resp.body);
        List<User> list = List<User>();
        for (var i in data) {
          list.add(User.fromJson(i));
        }
        this.data = list;
        this.mounted ?? setState(() {});
        calculateInformations();

        if (searchState.currentState != null)
          searchState.currentState.setData(list);
      }
    } catch (e) {
      print(e);
    } finally {
      setState(() {
        loading = false;
      });
      if (searchState.currentState != null)
        searchState.currentState.setLoading(this.loading);
    }
  }

  List<User> getFilterSudahMasuk(List<User> list) {
    return list.where((d) => d.attendance != null).toList();
  }

  List<User> getFilterBelumMasuk(List<User> list) {
    return list.where((d) => d.attendance == null).toList();
  }

  List<User> getFilterSudahKeluar(List<User> list) {
    return list
        .where((d) => d.attendance != null && d.attendance.exitDate != null)
        .toList();
  }

  List<User> getFilterIzin(List<User> list, String type) {
    return list.where((d) {
      if (d.permit == null) return false;
      if (type != null)
        return d.permit.permitType == type;
      else
        return true;
    }).toList();
  }

  Future<void> gotoList(String select, {String division}) async {
    List<User> selection;
    if (division == null)
      selection = this.data;
    else
      selection = this.data.where((d) => d.divisionId == division).toList();

    selection.sort((u, s) {
      if (u.attendance == null) return -2;
      if (s.attendance == null) return 2;
      return u.attendance.updatedAt.millisecondsSinceEpoch >
              s.attendance.updatedAt.millisecondsSinceEpoch
          ? 1
          : -1;
    });

    if (select == 'sudahmasuk') {
      Navigator.push(
          this.context,
          MaterialPageRoute(
              builder: (c) => ListAbsensi(getFilterSudahMasuk(selection))));
    }
    if (select == 'belummasuk') {
      Navigator.push(
          this.context,
          MaterialPageRoute(
              builder: (c) => ListAbsensi(getFilterBelumMasuk(selection))));
    }
  }

  Future<void> calculateInformations() async {
    var completer = Completer();
    /** Calculate guru */
    var data = this.data.where((d) => d.divisionId == 'guru').toList();
    var dataGuru = DashboardSumData();
    dataGuru.sudahMasuk = data.where((d) => d.attendance != null).length;
    dataGuru.belumMasuk = data.where((d) => d.attendance == null).length;
    dataGuru.izin = data
        .where((d) => d.permit != null && d.permit.permitType == 'izin')
        .length;
    dataGuru.izinLain = data
        .where((d) => d.permit != null && d.permit.permitType == 'izin_lain')
        .length;
    dataGuru.cuti = data
        .where((d) => d.permit != null && d.permit.permitType == 'cuti')
        .length;
    dataGuru.cutiLain = data
        .where((d) => d.permit != null && d.permit.permitType == 'cuti_lain')
        .length;
    dataGuru.sakit = data
        .where((d) => d.permit != null && d.permit.permitType == 'sakit')
        .length;

    List<CircularStackEntry> nextDataGuru = <CircularStackEntry>[
      new CircularStackEntry(
        <CircularSegmentEntry>[
          new CircularSegmentEntry(dataGuru.sudahMasuk.toDouble(), Colors.green,
              rankKey: 'Sudah Masuk'),
          new CircularSegmentEntry(dataGuru.belumMasuk.toDouble(), Colors.red,
              rankKey: 'Belum Masuk'),
          new CircularSegmentEntry(dataGuru.izin.toDouble(), Colors.yellow,
              rankKey: 'Dispensasi'),
        ],
        rankKey: 'Absensi',
      ),
    ];
    setState(() {
      chartGuru.currentState.updateData(nextDataGuru);
    });

    /** Data TU */
    data = this.data.where((d) => d.divisionId == "tata_usaha").toList();
    var dataTU = DashboardSumData();
    dataTU.sudahMasuk = data.where((d) => d.attendance != null).length;
    dataTU.belumMasuk = data.where((d) => d.attendance == null).length;
    dataTU.izin = data
        .where((d) => d.permit != null && d.permit.permitType == 'izin')
        .length;
    dataTU.izinLain = data
        .where((d) => d.permit != null && d.permit.permitType == 'izin_lain')
        .length;
    dataTU.cuti = data
        .where((d) => d.permit != null && d.permit.permitType == 'cuti')
        .length;
    dataTU.cutiLain = data
        .where((d) => d.permit != null && d.permit.permitType == 'cuti_lain')
        .length;
    dataTU.sakit = data
        .where((d) => d.permit != null && d.permit.permitType == 'sakit')
        .length;
    List<CircularStackEntry> nextDataTU = <CircularStackEntry>[
      new CircularStackEntry(
        <CircularSegmentEntry>[
          new CircularSegmentEntry(dataTU.sudahMasuk.toDouble(), Colors.green,
              rankKey: 'Sudah Masuk'),
          new CircularSegmentEntry(dataTU.belumMasuk.toDouble(), Colors.red,
              rankKey: 'Belum Masuk'),
          new CircularSegmentEntry(dataTU.izin.toDouble(), Colors.yellow,
              rankKey: 'Dispensasi'),
        ],
        rankKey: 'Absensi',
      ),
    ];
    setState(() {
      chartTU.currentState.updateData(nextDataTU);
    });
    setState(() {
      this.dataGuru = dataGuru;
      this.dataTU = dataTU;
    });
    return completer.future;
  }

  void created() async {
    await configs.transaction(this.db, (db) async {
      var completer = Completer();
      List<Map> dbdata = await db.find({'type': 'logindata'});
      final adminData = dbdata.first['logindata'];
      String token = dbdata.first['accesstoken'];
      if (adminData['admin'] != null) {
        AdminData tempUData = AdminData.fromJson(adminData['admin']);
        setState(() {
          this.adminData = tempUData;
          this.token = token;
        });
      }
      completer.complete(db);
      return completer.future;
    });
  }

  void logout() async {
    await configs.transaction(this.db, (db) async {
      await db.remove({"type": "logindata"});
      return Future.value(true);
    });

    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => Login()),
        ModalRoute.withName("./"));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    searchAnimationController =
        AnimationController(duration: Duration(milliseconds: 400), vsync: this);

    searchTextController.addListener(() {
      searchState.currentState.loadSearch(searchTextController.text);
    });
    configs.getDBPath().then((path) {
      db = ObjectDB(path);
      if (adminData == null || token == null) created();
    });

    loadData();
    this.timerLoader = Timer.periodic(Duration(minutes: 1), (timer) {
      loadData();
    });
    this.timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        now = DateTime.now();
      });
    });
  }

  @override
  void deactivate() {
    // TODO: implement deactivate
    this.timer.cancel();
    this.timerLoader.cancel();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey[500],
        title: Builder(
          builder: (c) {
            if (isSearching)
              return SearchText(
                  this.searchTextController, searchAnimationController);
            else
              return Text("Administrator");
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              if (searchAnimationController.isCompleted) {
                searchAnimationController.reverse().whenComplete(() {
                  setState(() {
                    isSearching = false;
                  });
                });
              } else {
                setState(() {
                  isSearching = true;
                });
                searchAnimationController.forward().whenComplete(() {
                  searchState.currentState.isLoading = this.loading;
                  searchState.currentState.setData(this.data);
                  searchState.currentState
                      .loadSearch(this.searchTextController.text);
                });
              }
            },
          ),
          FlatButton(
            child: Icon(
              Icons.refresh,
              color: Colors.white,
            ),
            onPressed: () {
              created();
            },
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: widgetBody(),
          ),
          Builder(
            builder: (c) {
              if (isSearching) {
                return SearchBar(searchAnimationController, searchState);
              } else
                return Positioned(
                  child: Container(),
                );
            },
          ),
        ],
      ),
    );
    //
  }

  Widget widgetBody() {
    return Column(
      children: <Widget>[
        Container(
          child: Column(
            children: <Widget>[
              Builder(
                builder: (c) {
                  if (this.adminData != null)
                    return HeaderRow(this.adminData);
                  else
                    return Container();
                },
              ),
              Container(
                padding: EdgeInsets.all(14),
                color: Colors.blue,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text(
                      "Absensi Hari Ini",
                      style: TextStyle(color: Colors.white),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                      child: Builder(
                        builder: (c) {
                          if (loading)
                            return SizedBox(
                              width: 15,
                              height: 15,
                              child: CircularProgressIndicator(
                                strokeWidth: 2,
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(Colors.red),
                              ),
                            );
                          else
                            return GestureDetector(
                              child: Icon(Icons.refresh),
                              onTap: () => loadData(),
                            );
                        },
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: Text(
                          DateFormat("dd MMM yyyy HH:mm:ss").format(now),
                          textAlign: TextAlign.end,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        /** Table Absensi */
        // AbsensiRow(
        //   null,
        //   isHeader: true,
        // ),
        // Expanded(
        //   child: Container(
        //     child: ListView.builder(
        //       itemCount: data.length,
        //       itemBuilder: (context, i) {
        //         return AbsensiRow(
        //           data[i],
        //           isHeader: false,
        //         );
        //       },
        //     ),
        //   ),
        // ),
        /** END Table Absensi */
        Expanded(
          child: ListView(
            children: <Widget>[
              Card(
                color: Colors.blueGrey[600],
                margin: EdgeInsets.all(6),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.only(bottom: 8),
                              child: Text(
                                "Tenaga Pendidik",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16),
                              ),
                            ),
                            Container(
                              child: RaisedButton(
                                onPressed: () {
                                  gotoList('sudahmasuk', division: "guru");
                                },
                                color: Colors.green,
                                child: Text(
                                  "${dataGuru.sudahMasuk} Sudah Masuk",
                                  style: const TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                            Container(
                              child: RaisedButton(
                                onPressed: () {
                                  gotoList('belummasuk', division: "guru");
                                },
                                color: Colors.red,
                                child: Text(
                                  "${dataGuru.belumMasuk} Belum Masuk",
                                  style: const TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                            Container(
                              child: RaisedButton(
                                onPressed: () {},
                                color: Colors.yellow,
                                child: Text(
                                  "0 Izin",
                                  style: const TextStyle(color: Colors.black),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    AnimatedCircularChart(
                      key: chartGuru,
                      edgeStyle: SegmentEdgeStyle.round,
                      size: const Size(120, 120),
                      holeLabel:
                          "${dataGuru.sudahMasuk}/${dataGuru.belumMasuk + dataGuru.sudahMasuk}",
                      labelStyle: TextStyle(color: Colors.white, fontSize: 21),
                      initialChartData: <CircularStackEntry>[
                        CircularStackEntry(<CircularSegmentEntry>[]),
                      ],
                      chartType: CircularChartType.Radial,
                    )
                  ],
                ),
              ),
              Card(
                color: Colors.blueGrey[600],
                margin: EdgeInsets.all(6),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.only(bottom: 8),
                              child: Text(
                                "Tenaga Kependidikan",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16),
                              ),
                            ),
                            Container(
                              child: RaisedButton(
                                onPressed: () {
                                  gotoList('sudahmasuk', division: "guru");
                                },
                                color: Colors.green,
                                child: Text(
                                  "${dataTU.sudahMasuk} Sudah Masuk",
                                  style: const TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                            Container(
                              child: RaisedButton(
                                onPressed: () {
                                  gotoList('belummasuk', division: "guru");
                                },
                                color: Colors.red,
                                child: Text(
                                  "${dataTU.belumMasuk} Belum Masuk",
                                  style: const TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                            Container(
                              child: RaisedButton(
                                onPressed: () {},
                                color: Colors.yellow,
                                child: Text(
                                  "${dataTU.izin} Izin",
                                  style: const TextStyle(color: Colors.black),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    AnimatedCircularChart(
                      key: chartTU,
                      edgeStyle: SegmentEdgeStyle.round,
                      size: const Size(120, 120),
                      holeLabel:
                          "${dataTU.sudahMasuk}/${dataTU.belumMasuk + dataTU.sudahMasuk}",
                      labelStyle: TextStyle(color: Colors.white, fontSize: 21),
                      initialChartData: <CircularStackEntry>[],
                      chartType: CircularChartType.Radial,
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(10),
          color: Colors.blue[600],
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              MenuItem(() {}, "Input Dispensasi", Icons.layers),
              MenuItem(() {
                Navigator.push(
                    this.context,
                    MaterialPageRoute(
                        builder: (c) => PermitRequest(
                            adminData: this.adminData, token: this.token)));
              }, "Acc Dispensasi", Icons.check_box)
            ],
          ),
        )
      ],
    );
  }
}

class SearchText extends StatelessWidget {
  final TextEditingController controller;
  final AnimationController displayAnimation;
  final Animation<double> animation;
  SearchText(this.controller, this.displayAnimation)
      : animation = Tween<double>(begin: 0, end: 1).animate(
            CurvedAnimation(parent: displayAnimation, curve: Curves.easeInOut));

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AnimatedBuilder(
      animation: displayAnimation,
      builder: (c, w) {
        return Opacity(
          opacity: animation.value,
          child: TextField(
            style: TextStyle(fontSize: 18),
            controller: controller,
            decoration: InputDecoration(
                fillColor: Colors.white,
                filled: true,
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)))),
          ),
        );
      },
    );
  }
}

class SearchBar extends StatefulWidget {
  GlobalKey<SearchBarState> key;
  SearchBar(this.searchAnimController, this.key) : super(key: key);
  Function onSearch;
  final AnimationController searchAnimController;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SearchBarState(searchAnimController);
  }
}

class SearchBarState extends State<SearchBar> {
  final AnimationController searchAnimController;
  final Animation<double> topSearchPost;
  final Animation<double> scaling;
  bool firstLoad = true;
  bool isLoading = true;
  String searchQuery = "";
  List<User> data = List<User>();

  SearchBarState(this.searchAnimController)
      : topSearchPost = Tween<double>(begin: 0, end: 1).animate(CurvedAnimation(
            parent: searchAnimController, curve: Curves.easeInOut)),
        scaling = Tween<double>(begin: 0.85, end: 1).animate(CurvedAnimation(
            parent: searchAnimController, curve: Curves.easeInOut));
  void setLoading(bool loading) {
    if (firstLoad) {
      setState(() {
        this.isLoading = loading;
        firstLoad = false;
      });
    }
  }

  void loadSearch(String query) {
    setState(() {
      this.searchQuery = query;
    });
  }

  void setData(List<User> data) {
    setState(() {
      this.data = data;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Widget list() {
    List<User> filterData;
    if (searchQuery == "" || searchQuery.length < 3)
      filterData = List<User>();
    else
      filterData = this
          .data
          .where((d) =>
              d.firstname.toLowerCase().contains(searchQuery.toLowerCase()))
          .toList();
    return Builder(
      builder: (b) {
        if (searchQuery == "" || searchQuery.length < 3)
          return Center(
            child: Text(
              "Enter a keyword of at least 3 letters...",
              style: TextStyle(color: Colors.grey[500]),
            ),
          );
        return ListView.separated(
          separatorBuilder: (c, i) {
            return Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(
                      bottom: BorderSide(color: Colors.grey, width: 0.5))),
            );
          },
          itemCount: filterData.length,
          itemBuilder: (c, i) {
            var data = filterData[i];
            return GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (c) => ProfileInfo(
                              userData: data,
                            )));
              },
              child: Container(
                color: i % 2 == 0 ? Colors.grey[200] : Colors.white,
                padding: EdgeInsets.all(6),
                child: Row(
                  children: <Widget>[
                    Center(
                      child: CachedNetworkImage(
                        width: 50,
                        imageUrl: configs.getPhotoUrl(data.id),
                        placeholder: (c, s) => CircularProgressIndicator(),
                        errorWidget: (c, s, e) => SizedBox(
                              width: 50,
                              child: Image(
                                image: AssetImage('images/smk.png'),
                              ),
                            ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(left: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              child: Text(data.firstname,
                                  style: TextStyle(fontSize: 18)),
                              padding: EdgeInsets.fromLTRB(4, 3, 0, 4),
                            ),
                            Padding(
                              child: Text(
                                  "${data.division.division} - ${data.employmentStatus.employmentStatus}",
                                  style: TextStyle(fontSize: 14)),
                              padding: EdgeInsets.fromLTRB(4, 3, 0, 4),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(4, 4, 4, 4),
                              child: Builder(
                                builder: (b) {
                                  if (data.attendance != null) {
                                    if (data.attendance.exitDate != null) {
                                      return Text(
                                        "Sudah Pulang - ${DateFormat('HH:mm').format(data.attendance.exitDate)}",
                                        style: const TextStyle(
                                            color: Colors.amber),
                                      );
                                    } else
                                      return Text(
                                        "Sudah Masuk - ${DateFormat('HH:mm').format(data.attendance.entryDate)}",
                                        style: const TextStyle(
                                            color: Colors.green),
                                      );
                                  } else {
                                    return Text(
                                      "Belum Masuk",
                                      style: const TextStyle(color: Colors.red),
                                    );
                                  }
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AnimatedBuilder(
      animation: searchAnimController,
      builder: (context, builder) {
        return Positioned(
          left: 10,
          right: 10,
          bottom: 10,
          top: topSearchPost.value,
          child: Transform.scale(
            scale: scaling.value,
            child: Opacity(
              opacity: topSearchPost.value,
              child: Card(
                child: Container(
                  padding: const EdgeInsets.all(20),
                  child: Builder(
                    builder: (c) {
                      if (this.isLoading)
                        return Center(
                          child: SizedBox(
                            width: 60,
                            height: 60,
                            child: CircularProgressIndicator(),
                          ),
                        );
                      else
                        return list();
                    },
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class HeaderRow extends StatelessWidget {
  final AdminData data;
  HeaderRow(this.data);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(10),
      color: Colors.blueGrey[600],
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(Icons.label_important, color: Colors.red),
                    Text("Important Persons",
                        style: TextStyle(color: Colors.white, fontSize: 17))
                  ],
                ),
                SizedBox(
                  height: 150,
                  child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3, childAspectRatio: 2),
                    itemBuilder: (c, i) {
                      return Center(
                        child: Container(
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Icon(
                                Icons.person,
                                color: Colors.white,
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 4),
                                child: Text(
                                  "User $i",
                                  style: TextStyle(color: Colors.white),
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                    itemCount: 14,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
