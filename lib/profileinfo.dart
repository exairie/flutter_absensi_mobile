import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:absensi_mobile/configs.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'models/data.dart';
import 'package:objectdb/objectdb.dart';
import 'package:absensi_mobile/homepage.dart';
import 'package:intl/intl.dart';
import 'package:absensi_mobile/jadwalkbm.dart';
import 'package:absensi_mobile/logabsensi.dart';

class ProfileInfo extends StatefulWidget {
  User userData;
  ProfileInfo({this.userData});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProfileInfoState();
  }
}

class ProfileInfoState extends State<ProfileInfo> {
  User userData;
  ObjectDB db;
  void created() async {
    await db.open();
    List<Map> dbdata = await db.find({'type': 'logindata'});
    final adminData = dbdata.first['logindata'];
    String token = dbdata.first['accesstoken'];
    await db.close();

    if (adminData['user'] != null) {
      User tempUData = User.fromJson(adminData['user']);

      setState(() {});
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.userData != null) {
      userData = widget.userData;
    } else {
      created();
    }
  }

  String enterHour() {
    return userData.attendance == null
        ? "-"
        : DateFormat("HH:mm").format(userData.attendance.entryDate);
  }

  String exitHour() {
    if (userData.attendance == null) {
      return "-";
    } else {
      if (userData.attendance.exitDate == null) {
        return "-";
      } else {
        return DateFormat("HH:mm").format(userData.attendance.exitDate);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Informasi Profil"),
      ),
      body: ListView(
        children: <Widget>[
          Card(
              margin: EdgeInsets.all(4),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(10),
                        child: CachedNetworkImage(
                          width: 80,
                          imageUrl: getPhotoUrl(userData.id),
                          placeholder: (c, s) => SizedBox(
                                width: 50,
                                height: 50,
                                child: CircularProgressIndicator(),
                              ),
                          errorWidget: (c, i, e) => Image(
                                image: AssetImage("images/smk.png"),
                                width: 80,
                              ),
                        ),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(bottom: 8),
                              child: Text(
                                userData.firstname,
                                style: TextStyle(
                                    color: Colors.grey[800],
                                    fontSize: 19,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.only(bottom: 4),
                                child: Text(
                                  userData.division.division,
                                  style: TextStyle(
                                    color: Colors.grey[800],
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal,
                                  ),
                                )),
                            Container(
                                margin: EdgeInsets.only(bottom: 4),
                                child: Text(
                                  userData.employmentStatus.employmentStatus,
                                  style: TextStyle(
                                    color: Colors.grey[800],
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal,
                                  ),
                                )),
                            Builder(
                              builder: (c) {
                                if (userData.importance != null) {
                                  return Container(
                                      margin: EdgeInsets.only(bottom: 8),
                                      child: Text(
                                        userData.importanceDetail,
                                        style: TextStyle(
                                          color: Colors.grey[800],
                                          fontSize: 15,
                                          fontWeight: FontWeight.normal,
                                        ),
                                      ));
                                } else {
                                  return Center();
                                }
                              },
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Icon(
                          Icons.today,
                          color: Colors.amber,
                        ),
                        Container(
                            padding: EdgeInsets.only(),
                            child: Text(
                              "Hari Ini",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              ),
                            )),
                        Container(
                            padding: EdgeInsets.only(),
                            child: Text(
                              "Masuk",
                              style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontSize: 14,
                              ),
                            )),
                        Container(
                            padding: EdgeInsets.only(),
                            child: Text(
                              enterHour(),
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 20,
                                  color: Colors.green),
                            )),
                        Container(
                            padding: EdgeInsets.only(),
                            child: Text(
                              "Keluar",
                              style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontSize: 14,
                              ),
                            )),
                        Container(
                            padding: EdgeInsets.only(),
                            child: Text(
                              exitHour(),
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 20,
                                  color: Colors.green),
                            ))
                      ],
                    ),
                  )
                ],
              )),
          Card(
            margin: EdgeInsets.all(4),
            child: Container(
              padding: EdgeInsets.all(4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(5),
                    padding: const EdgeInsets.only(left: 6),
                    child: Text("Detil Pegawai"),
                    decoration: BoxDecoration(
                        border: BorderDirectional(
                            start: BorderSide(color: Colors.blue, width: 4))),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      ButtonWidget(() {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (c) => LogAbsensi(
                                      userData: userData,
                                    )));
                      }, "Log\nAbsensi", Icons.history),
                      ButtonWidget(() {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (c) => JadwalKBM(
                                      userdata: userData,
                                    )));
                      }, "Jadwal\nKBM", Icons.schedule),
                      ButtonWidget(() {}, "Panggil", Icons.call),
                    ],
                  )
                ],
              ),
            ),
          ),
          Card(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(5),
                      padding: EdgeInsets.only(left: 6),
                      child: Text("Jam Kerja"),
                      decoration: BoxDecoration(
                          border: BorderDirectional(
                              start: BorderSide(color: Colors.blue, width: 4))),
                    ),
                  ],
                ),
                TotalInWeek(userData),
                Container(
                  margin: const EdgeInsets.only(bottom: 8, left: 6),
                  decoration: BoxDecoration(
                      border: BorderDirectional(
                          start: BorderSide(color: Colors.amber, width: 4))),
                  alignment: Alignment.centerLeft,
                  padding: const EdgeInsets.only(left: 6),
                  child: const Text("Jam Masuk"),
                ),
                Container(
                  child: InfoEntrance(userData),
                  margin: const EdgeInsets.only(bottom: 4),
                )
              ],
            ),
          ),
          Card(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.all(5),
                      padding: const EdgeInsets.only(left: 6),
                      child: const Text("Kontak"),
                      decoration: BoxDecoration(
                          border: BorderDirectional(
                              start: BorderSide(color: Colors.blue, width: 4))),
                    ),
                  ],
                ),
                Container(
                    padding: const EdgeInsets.only(left: 10, top: 10),
                    child: Text(
                      "No HP",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                    )),
                Container(
                    padding: const EdgeInsets.only(left: 10, top: 5, bottom: 8),
                    child: Text(
                      userData.phone,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class InfoEntrance extends StatelessWidget {
  final User user;
  InfoEntrance(this.user);
  List<String> getHour(String day) {
    var hrs = List<String>();
    DateTime enter, exit;
    if (user.personalSchedule != null) {
      switch (day) {
        case "senin":
          {
            if (user.personalSchedule.seninCustom == 1) {
              enter = createDateTimeFromHM(user.personalSchedule.seninMasukJam,
                  user.personalSchedule.seninMasukMenit);
              exit = createDateTimeFromHM(user.personalSchedule.seninKeluarJam,
                  user.personalSchedule.seninKeluarMenit);
            } else {
              enter = createDateTimeFromHM(
                  user.division.shiftStartHour, user.division.shiftStartMinute);
              exit = createDateTimeFromHM(
                  user.division.shiftEndHour, user.division.shiftEndMinute);
            }
          }
          break;
        case "selasa":
          {
            if (user.personalSchedule.selasaCustom == 1) {
              enter = createDateTimeFromHM(user.personalSchedule.selasaMasukJam,
                  user.personalSchedule.selasaMasukMenit);
              exit = createDateTimeFromHM(user.personalSchedule.selasaKeluarJam,
                  user.personalSchedule.selasaKeluarMenit);
            } else {
              enter = createDateTimeFromHM(
                  user.division.shiftStartHour, user.division.shiftStartMinute);
              exit = createDateTimeFromHM(
                  user.division.shiftEndHour, user.division.shiftEndMinute);
            }
          }
          break;
        case "rabu":
          {
            if (user.personalSchedule.rabuCustom == 1) {
              enter = createDateTimeFromHM(user.personalSchedule.rabuMasukJam,
                  user.personalSchedule.rabuMasukMenit);
              exit = createDateTimeFromHM(user.personalSchedule.rabuKeluarJam,
                  user.personalSchedule.rabuKeluarMenit);
            } else {
              enter = createDateTimeFromHM(
                  user.division.shiftStartHour, user.division.shiftStartMinute);
              exit = createDateTimeFromHM(
                  user.division.shiftEndHour, user.division.shiftEndMinute);
            }
          }
          break;
        case "kamis":
          {
            if (user.personalSchedule.kamisCustom == 1) {
              enter = createDateTimeFromHM(user.personalSchedule.kamisMasukJam,
                  user.personalSchedule.kamisMasukMenit);
              exit = createDateTimeFromHM(user.personalSchedule.kamisKeluarJam,
                  user.personalSchedule.kamisKeluarMenit);
            } else {
              enter = createDateTimeFromHM(
                  user.division.shiftStartHour, user.division.shiftStartMinute);
              exit = createDateTimeFromHM(
                  user.division.shiftEndHour, user.division.shiftEndMinute);
            }
          }
          break;
        case "jumat":
          {
            if (user.personalSchedule.jumatCustom == 1) {
              enter = createDateTimeFromHM(user.personalSchedule.jumatMasukJam,
                  user.personalSchedule.jumatMasukMenit);
              exit = createDateTimeFromHM(user.personalSchedule.jumatKeluarJam,
                  user.personalSchedule.jumatKeluarMenit);
            } else {
              enter = createDateTimeFromHM(
                  user.division.shiftStartHour, user.division.shiftStartMinute);
              exit = createDateTimeFromHM(
                  user.division.shiftEndHour, user.division.shiftEndMinute);
            }
          }
          break;
      }
    } else {
      enter = createDateTimeFromHM(
          user.division.shiftStartHour, user.division.shiftStartMinute);
      exit = createDateTimeFromHM(
          user.division.shiftEndHour, user.division.shiftEndMinute);
    }

    var format = DateFormat("HH:mm");
    return <String>[
      format.format(enter),
      format.format(exit),
    ];
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        EntranceWindow(getHour("senin"), "senin"),
        EntranceWindow(getHour("selasa"), "selasa"),
        EntranceWindow(getHour("rabu"), "rabu"),
        EntranceWindow(getHour("kamis"), "kamis"),
        EntranceWindow(getHour("jumat"), "jumat"),
      ],
    );
  }
}

class EntranceWindow extends StatelessWidget {
  final String hourEnter, hourExit;
  final String day;
  EntranceWindow(List<String> times, this.day)
      : hourEnter = times[0],
        hourExit = times[1];
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
            padding: const EdgeInsets.only(left: 4, right: 4, bottom: 2),
            margin: const EdgeInsets.only(bottom: 2),
            child: Text(day.toUpperCase()),
            decoration: BoxDecoration(
              border: BorderDirectional(
                  bottom: BorderSide(color: Colors.amber[500], width: 2)),
            )),
        Text(
          "Masuk",
          style: TextStyle(fontSize: 10),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 4),
          child: Text(hourEnter,
              style: TextStyle(
                fontSize: 19,
              )),
        ),
        Text("Keluar", style: TextStyle(fontSize: 10)),
        Text(hourExit,
            style: TextStyle(
              fontSize: 19,
            )),
      ],
    );
  }
}

class TotalInWeek extends StatefulWidget {
  final User userData;
  TotalInWeek(this.userData);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return TotalInWeekState();
  }
}

class TotalInWeekState extends State<TotalInWeek> {
  bool loading = false;
  User userdata;
  List<AttendanceLogs> logs = List<AttendanceLogs>();
  String totalminus = "-", totalthisweek = "-", expected = "-";
  int whExpected = 0, whTotal = 0, whMinus = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.userdata = widget.userData;
    loadData();
  }

  void loadData() async {
    setState(() {
      loading = true;
    });
    var now = DateTime.now();
    var from = now.subtract(Duration(days: now.weekday - 1));
    var to = from.add(Duration(days: 5));
    var format = DateFormat("yyyy-MM-dd");
    try {
      var resp = await http.get(
          "${serverUrl}/api/enrollment/${userdata.id}/attendance?from=${format.format(from)}&to=${format.format(to)}");
      if (resp.statusCode == 200) {
        dynamic data = json.decode(resp.body);
        List<AttendanceLogs> tList = List<AttendanceLogs>();
        for (var att in data['logs']) {
          tList.add(AttendanceLogs.fromJson(att));
        }

        setState(() {
          logs = tList;
        });

        calculate();
      }
    } catch (e) {
      print(e);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  void calculate() {
    var expectedWorkHour = userdata.getExpectedWorkHour();
    int totalWorkHour = 0;
    for (var att in logs) {
      totalWorkHour += att.getRealWorkTime(userdata.division);
    }

    setState(() {
      whTotal = totalWorkHour;
      whMinus = totalWorkHour - expectedWorkHour;
      whExpected = expectedWorkHour;
      totalthisweek = toDateTimeString(totalWorkHour);
      totalminus = toDateTimeString((totalWorkHour - expectedWorkHour).abs());
      expected = toDateTimeString(expectedWorkHour);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: const EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                child: Builder(
                  builder: (c) {
                    if (loading)
                      return SizedBox(
                        height: 30,
                        width: 30,
                        child: CircularProgressIndicator(),
                      );
                    else
                      return Text(
                        totalthisweek,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 17),
                      );
                  },
                ),
                margin: const EdgeInsets.only(bottom: 4),
              ),
              Text(
                "Total Minggu Ini",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 12),
              )
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                child: Builder(
                  builder: (c) {
                    if (loading)
                      return SizedBox(
                        height: 30,
                        width: 30,
                        child: CircularProgressIndicator(),
                      );
                    else
                      return Text(
                        expected,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 17),
                      );
                  },
                ),
                margin: const EdgeInsets.only(bottom: 4),
              ),
              Text(
                "Seharusnya",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 12),
              )
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                child: Builder(
                  builder: (c) {
                    if (loading)
                      return SizedBox(
                        height: 30,
                        width: 30,
                        child: CircularProgressIndicator(),
                      );
                    else
                      return Text(
                        totalminus,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 17,
                            color: whMinus < 0 ? Colors.red : Colors.green),
                      );
                  },
                ),
                margin: const EdgeInsets.only(bottom: 4),
              ),
              Text(
                (whMinus < 0 ? "Kekurangan Jam" : "Kelebihan Jam"),
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 12,
                    color: whMinus < 0 ? Colors.red : Colors.green),
              )
            ],
          )
        ],
      ),
    );
  }
}
