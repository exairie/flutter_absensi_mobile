import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:absensi_mobile/models/data.dart';
import 'package:objectdb/objectdb.dart';
import 'package:http/http.dart' as http;
import 'package:absensi_mobile/configs.dart' as configs;
import 'package:moment/moment.dart';
import 'dart:async';
import 'dart:convert';
import 'package:absensi_mobile/loginscreen.dart';

class LogAbsensiState extends State<LogAbsensi> {
  bool loading = false;
  DateTime from = DateTime(DateTime.now().year, DateTime.now().month - 1);
  DateTime to = DateTime.now();
  ObjectDB db;
  User userData;
  Function userDataChanged;
  List<Permit> permits = List<Permit>();
  List<AttendanceLogs> logs = List<AttendanceLogs>();
  void created() async {
    await db.open();
    List<Map> dbdata = await db.find({'type': 'logindata'});
    final adminData = dbdata.first['logindata'];
    if (adminData['user'] != null) {
      User tempUData = User.fromJson(adminData['user']);
      this.userData = tempUData;
      if (userDataChanged != null) {
        userDataChanged(tempUData);
      }
      if (adminData['logintype'] == 'user') {
        // Disable, use callback instead

        fetchAttendanceLogs(tempUData.id.toString());
      }
    }
    await db.close();
  }

  fetchAttendanceLogs(String enrollID) async {
    var queryParam = {
      "from": DateFormat("yyyy-MM-dd").format(from),
      "to": DateFormat("yyyy-MM-dd").format(to)
    };
    setState(() {
      loading = true;
    });
    Uri uri;
    if (configs.serverUrl.contains("https")) {
      uri = Uri.https(
          configs.serverUrl
              .replaceAll("https://", "")
              .replaceAll("http://", ""),
          "/api/enrollment/${enrollID}/attendance",
          queryParam);
    } else {
      uri = Uri.http(
          configs.serverUrl
              .replaceAll("https://", "")
              .replaceAll("http://", ""),
          "/api/enrollment/${enrollID}/attendance",
          queryParam);
    }
    final response = await http.get(uri);
    if (response.statusCode == 200) {
      dynamic data = json.decode(response.body);
      List<AttendanceLogs> tList = List<AttendanceLogs>();
      for (var att in data['logs']) {
        tList.add(AttendanceLogs.fromJson(att));
      }
      tList = tList
          .where((d) =>
              d.entryDate.weekday != DateTime.sunday &&
              d.entryDate.weekday != DateTime.saturday)
          .toList();
      tList.sort((a, b) {
        int diff = a.entryDate.difference(b.entryDate).inMicroseconds;
        return diff > 0 ? -1 : diff < 0 ? 1 : 0;
      });
      List<Permit> permits = List<Permit>();
      for (var p in data['permits']) {
        permits.add(Permit.fromJson(p));
      }
      setState(() {
        logs = tList;
        this.permits = permits;
        loading = false;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    configs.getDBPath().then((v) async {
      db = ObjectDB(v);

      if (widget.userData == null)
        created();
      else {
        this.userData = widget.userData;
        userDataChanged(userData);
        fetchAttendanceLogs(userData.id.toString());
      }
    });
  }

  @override
  void deactivate() {
    // TODO: implement deactivate
    super.deactivate();
  }

  @override
  void dispose() async {
    // TODO: implement dispose
    super.dispose();
    // await db.close();
  }

  void loadLogAbsensi() async {}
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(loading ? "Loading..." : "Riwayat Absensi"),
              Text(
                userData == null ? "Loading..." : userData.firstname,
                style: TextStyle(fontSize: 13),
              ),
            ],
          ),
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              LogHeaderState(
                Object(),
                (from, to) {
                  setState(() {
                    this.from = from;
                    this.to = to;
                  });
                  if (userData != null)
                    fetchAttendanceLogs(userData.id.toString());
                },
                parent: this,
              ),
              ContentTableRow(Object(), boldText: true),
              Expanded(
                  child: ListView.builder(
                itemCount: logs.length,
                itemBuilder: (context, row) {
                  var log = logs[row];
                  var permit = permits.firstWhere((p) {
                    return p.permitFrom.millisecondsSinceEpoch <=
                            log.entryDate.millisecondsSinceEpoch &&
                        log.entryDate.millisecondsSinceEpoch <=
                            p.permitTo.millisecondsSinceEpoch;
                  }, orElse: () {
                    return null;
                  });
                  if (permit != null) {
                    return PermitRow(
                      permit: permit,
                      row: row,
                      date: log.entryDate,
                    );
                  }
                  return ContentTableRow(logs[row], row: row);
                },
              ))
            ],
          ),
        ));
  }
}

class LogAbsensi extends StatefulWidget {
  User userData;
  LogAbsensi({this.userData});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LogAbsensiState();
  }
}

class ContentTableRow extends StatelessWidget {
  static const double contentpadding = 9;
  bool isBoldText = false;
  int row = 0;
  AttendanceLogs data;
  ContentTableRow(Object data, {bool boldText = false, this.row}) {
    isBoldText = boldText;
    if (data is AttendanceLogs) {
      this.data = data;
    }
  }

  @override
  Widget build(BuildContext context) {
    double totalWidth = MediaQuery.of(context).size.width;
    // TODO: implement build
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          SizedBox(
            width: 0.1 * totalWidth,
            child: Padding(
              padding: const EdgeInsets.all(contentpadding),
              child: Text(data == null ? "No" : (row + 1).toString(),
                  style: TextStyle(
                      fontWeight:
                          isBoldText ? FontWeight.w700 : FontWeight.normal)),
            ),
          ),
          SizedBox(
            width: 0.4 * totalWidth,
            child: Padding(
              padding: const EdgeInsets.all(contentpadding),
              child: Builder(
                builder: (c) {
                  if (data == null) {
                    return Text("Tanggal",
                        style: TextStyle(
                            fontWeight: isBoldText
                                ? FontWeight.w700
                                : FontWeight.normal));
                  } else {
                    try {
                      return Text(
                          DateFormat("E, dd MMM yyyy").format(data.entryDate),
                          style: TextStyle(
                              fontWeight: isBoldText
                                  ? FontWeight.w700
                                  : FontWeight.normal));
                    } catch (e) {
                      return Text("Invalid Date");
                    }
                  }
                },
              ),
            ),
          ),
          SizedBox(
            width: 0.25 * totalWidth,
            child: Padding(
              padding: const EdgeInsets.all(contentpadding),
              child: Builder(
                builder: (c) {
                  if (data == null) {
                    return Text("Masuk",
                        style: TextStyle(
                            fontWeight: isBoldText
                                ? FontWeight.w700
                                : FontWeight.normal));
                  } else {
                    try {
                      return Text(DateFormat("HH:mm").format(data.entryDate),
                          style: TextStyle(
                              fontWeight: isBoldText
                                  ? FontWeight.w700
                                  : FontWeight.normal));
                    } catch (e) {
                      return Text("Invalid Date");
                    }
                  }
                },
              ),
            ),
          ),
          SizedBox(
            width: 0.25 * totalWidth,
            child: Padding(
              padding: const EdgeInsets.all(contentpadding),
              child: Builder(
                builder: (c) {
                  if (data == null) {
                    return Text("Keluar",
                        style: TextStyle(
                            fontWeight: isBoldText
                                ? FontWeight.w700
                                : FontWeight.normal));
                  } else if (data != null && data.exitDate == null) {
                    return Text("Belum keluar",
                        style: TextStyle(
                            fontWeight: isBoldText
                                ? FontWeight.w700
                                : FontWeight.normal));
                  } else {
                    try {
                      return Text(DateFormat("HH:mm").format(data.exitDate),
                          style: TextStyle(
                              fontWeight: isBoldText
                                  ? FontWeight.w700
                                  : FontWeight.normal));
                    } catch (e) {
                      return Text("Invalid Date");
                    }
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class PermitRow extends StatelessWidget {
  static const double contentpadding = 9;
  bool isBoldText = false;
  int row = 0;
  AttendanceLogs data;
  Permit permit;
  DateTime date;
  PermitRow({bool boldText = false, this.row, this.permit, this.date}) {
    isBoldText = boldText;
  }

  @override
  Widget build(BuildContext context) {
    double totalWidth = MediaQuery.of(context).size.width;
    // TODO: implement build
    return Container(
      color: Colors.amber[800],
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          SizedBox(
            width: 0.1 * totalWidth,
            child: Padding(
              padding: const EdgeInsets.all(contentpadding),
              child: Text((row + 1).toString(),
                  style: TextStyle(
                      fontWeight:
                          isBoldText ? FontWeight.w700 : FontWeight.normal)),
            ),
          ),
          SizedBox(
            width: 0.4 * totalWidth,
            child: Padding(
              padding: const EdgeInsets.all(contentpadding),
              child: Text(DateFormat("E, dd MMM yyyy").format(date),
                  style: TextStyle(
                      fontWeight:
                          isBoldText ? FontWeight.w700 : FontWeight.normal)),
            ),
          ),
          SizedBox(
            width: 0.5 * totalWidth,
            child: Padding(
              padding: const EdgeInsets.all(contentpadding),
              child: Text(permit.reason,
                  style: TextStyle(
                      fontWeight:
                          isBoldText ? FontWeight.w700 : FontWeight.normal)),
            ),
          ),
        ],
      ),
    );
  }
}

class LogHeaderState extends StatefulWidget {
  bool loading = false;
  Object data;
  LogAbsensiState parent;
  Function dateChangeCallback;
  LogHeaderState(Object data, Function dateChangeCallback, {this.parent}) {
    this.data = data;
    this.dateChangeCallback = dateChangeCallback;
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new LogHeader(data, dateChangeCallback, parent: this.parent);
  }
}

class LogHeader extends State<LogHeaderState> {
  Object data;
  Function dateChangeCallback;
  User user;
  LogAbsensiState parent;
  DateTime selectedTimeFrom =
      DateTime(DateTime.now().year, DateTime.now().month - 1);
  DateTime selectedTimeTo = DateTime.now();
  LogHeader(Object data, Function dateChangeCallback, {this.parent}) {
    this.data = data;
    this.dateChangeCallback = dateChangeCallback;
    this.parent.userDataChanged = (udata) {
      setState(() {
        user = udata;
      });
    };
  }
  void changeDate(BuildContext c, String type) async {
    if (type == "from") {
      final DateTime pickerfrom = await showDatePicker(
          context: c,
          initialDate: selectedTimeFrom,
          firstDate: DateTime(DateTime.now().year - 2),
          lastDate: DateTime(DateTime.now().year + 1));

      if (pickerfrom != null && pickerfrom != selectedTimeFrom) {
        setState(() {
          selectedTimeFrom = pickerfrom;
          // selectedTimeTo = pickerto;
        });
      }
    }
    if (type == "to") {
      final DateTime pickerto = await showDatePicker(
          context: c,
          initialDate: selectedTimeFrom,
          firstDate: DateTime(DateTime.now().year - 2),
          lastDate: DateTime(DateTime.now().year + 1));
      if (pickerto != null && pickerto != selectedTimeFrom) {
        setState(() {
          selectedTimeTo = pickerto;
          // selectedTimeTo = pickerto;
        });
      }
    }
    dateChangeCallback(selectedTimeFrom, selectedTimeTo);
  }

  @override
  void initState() {
    super.initState();
  }

  Widget datePeriode() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
              onTap: () {
                changeDate(context, "from");
              },
              child: Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  color: Colors.grey[300],
                ),
                child: Text(
                  DateFormat("dd MMM yyyy").format(selectedTimeFrom),
                  style: TextStyle(fontWeight: FontWeight.w800),
                ),
              )),
          Text(
            " - ",
            style: TextStyle(color: Colors.white),
          ),
          GestureDetector(
              onTap: () {
                changeDate(context, "to");
              },
              child: Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  color: Colors.grey[300],
                ),
                child: Text(
                  DateFormat("dd MMM yyyy").format(selectedTimeTo),
                  style: TextStyle(fontWeight: FontWeight.w800),
                ),
              ))
        ],
      ),
      padding: EdgeInsets.fromLTRB(0, 4, 0, 4),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.all(16),
      color: Colors.blue[600],
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(10, 0, 20, 0),
            child: Icon(
              Icons.history,
              color: Colors.white,
              size: 50,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 4),
                  child: Text(
                    "Log Absensi",
                    style: TextStyle(fontSize: 19, color: Colors.white),
                  )),
              // Container(
              //     margin: EdgeInsets.fromLTRB(0, 0, 0, 4),
              //     child: Text(
              //       user != null ? user.divisionId : "-",
              //       style: TextStyle(fontSize: 16, color: Colors.white),
              //     )),
              datePeriode(),
              // Container(
              //     margin: EdgeInsets.fromLTRB(0, 0, 0, 4),
              //     child: Text(
              //       "Jam kerja : 24 Jam",
              //       style: TextStyle(fontSize: 14, color: Colors.white),
              //     )),
            ],
          )
        ],
      ),
    );
  }
}
