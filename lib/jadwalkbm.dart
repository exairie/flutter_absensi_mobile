import 'dart:core';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'dart:async';
import 'dart:convert';
import 'package:objectdb/objectdb.dart';
import 'package:http/http.dart' as http;
import 'package:absensi_mobile/models/data.dart';
import 'package:absensi_mobile/configs.dart' as configs;

class KBM {
  HourDefinition hour;
  EnrollmentSchedules schedule;
  String day;
  KBM({this.hour, this.schedule, this.day});
}

class JadwalKBM extends StatefulWidget {
  User userdata;
  JadwalKBM({this.userdata});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _JadwalKBM();
  }
}

class _JadwalKBM extends State<JadwalKBM> {
  ObjectDB db;
  User userData;
  bool loading = false;
  List<EnrollmentSchedules> schedules = List<EnrollmentSchedules>();
  List<KBM> kbm = List<KBM>();
  int currentHour = 0;
  Timer timer;
  var days = ['senin', 'selasa', 'rabu', 'kamis', 'jumat'];
  @override
  void dispose() {
    // TODO: implement dispose
    if (this.timer != null) this.timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    configs.getDBPath().then((v) async {
      db = ObjectDB(v);
      await db.open();

      if (widget.userdata != null) {
        this.userData = widget.userdata;
        loadSchedules();
      } else
        created();
    });
  }

  void created() async {
    List<Map> dbdata = await db.find({'type': 'logindata'});
    final adminData = dbdata.first['logindata'];
    if (adminData['user'] != null) {
      User tempUData = User.fromJson(adminData['user']);
      setState(() {
        this.userData = tempUData;
      });
      loadSchedules();
    }
    this.timer = Timer.periodic(Duration(minutes: 1), (t) {
      setState(() {});
    });
  }

  int dayOrder(String day) {
    switch (day) {
      case "senin":
        return 0;
        break;
      case "selasa":
        return 1;
        break;
      case "rabu":
        return 2;
        break;
      case "kamis":
        return 3;
        break;
      case "jumat":
        return 4;
        break;
      default:
    }
  }

  void loadSchedules() async {
    try {
      setState(() {
        loading = true;
      });
      var resp = await http
          .get("${configs.serverUrl}/api/schedules/${userData.id.toString()}");
      if (resp.statusCode == 200) {
        dynamic data = json.decode(resp.body);
        List<EnrollmentSchedules> schedules = List<EnrollmentSchedules>();
        var hours = List<HourDefinition>();
        var kbms = List<KBM>();
        for (var i in data['data']) {
          schedules.add(EnrollmentSchedules.fromJson(i));
        }
        schedules.sort((a, b) {
          var d1 = dayOrder(a.day);
          var d2 = dayOrder(b.day);
          return d1 > d2 ? 1 : d1 < d2 ? -1 : 0;
        });
        for (var day in days) {
          for (var i in data['hour']) {
            var h = HourDefinition.fromJson(i);
            hours.add(h);
            var sch;
            try {
              sch = schedules
                  .where((e) => e.hour == h.hour && e.day == day)
                  .first;
            } catch (e) {
              sch = null;
            }
            var kbm = KBM(hour: h, schedule: sch, day: day);
            kbms.add(kbm);
          }
        }

//        kbms.sort((a,b) {
//
//          return a.hour.hour > b.hour.hour?1:a.hour.hour< b.hour.hour ?-1:0;
//        });
        await db.open();
        var hourInfoList = await db.find({'type': 'hourinfo'});
        if (hourInfoList.length > 1) {
          var hourInfoData = hourInfoList.first;
          if (hourInfoData != null) {
            setState(() {
              currentHour = hourInfoData['hour'] as int;
            });
          }
        }
        setState(() {
          this.kbm = kbms;
        });
      }
    } catch (e) {
      print(e);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var initIndex = DateTime.now().weekday - 1;
    if (initIndex > 4) initIndex = 0;
    return DefaultTabController(
      initialIndex: initIndex,
      length: 5,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(tabs: [
            Tab(
              text: "Senin",
            ),
            Tab(
              text: "Selasa",
            ),
            Tab(
              text: "Rabu",
            ),
            Tab(
              text: "Kamis",
            ),
            Tab(
              text: "Jumat",
            ),
          ]),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Jadwal KBM"),
              Text(
                userData == null ? "-" : userData.firstname,
                style: TextStyle(fontSize: 13),
              ),
            ],
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () {
                loadSchedules();
              },
            )
          ],
        ),
        body: Builder(
          builder: (c) {
            if (loading)
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Container(
                      margin: EdgeInsets.all(10),
                      child: Text("Loading..."),
                    )
                  ],
                ),
              );
            else
              return TabBarView(
                children: <Widget>[
                  widgetBody(dayFilter: "senin"),
                  widgetBody(dayFilter: "selasa"),
                  widgetBody(dayFilter: "rabu"),
                  widgetBody(dayFilter: "kamis"),
                  widgetBody(dayFilter: "jumat"),
                ],
              );
          },
        ),
      ),
    );
  }

  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

  Widget widgetBody({String dayFilter = null}) {
    String lastDay = "";
    var list;
    if (dayFilter != null) {
      list = this.kbm.where((k) => k.day == dayFilter).toList();
    } else
      list = this.kbm;

    return ListView.builder(
//      separatorBuilder: (context, i) {
//
//      },
      itemCount: list.length + 1,
      itemBuilder: (context, i) {
//        Widget ret;
//        if (lastDay != list[i].day) {
//          ret = HeaderRow(title: capitalize(list[i].day));
//        } else
//          ret = Container();
//        lastDay = list[i].day;
//        return ret;
        if (i == 0) return TableHeader();
        return ContentRow(kbmData: list[i - 1]);
      },
    );
  }
}

class HeaderRow extends StatelessWidget {
  String title;
  HeaderRow({this.title});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.all(3),
      color: Colors.blue[600],
      child: Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(4),
            child: Icon(
              Icons.calendar_today,
              color: Colors.white,
            ),
          ),
          Text(
            title,
            style: TextStyle(fontSize: 19, color: Colors.white),
          )
        ],
      ),
    );
  }
}

class TableHeader extends StatelessWidget {
  KBM kbmData;
  TableHeader({this.kbmData});

  @override
  Widget build(BuildContext context) {
    double totalWidth = MediaQuery.of(context).size.width;
    var padding = 8.0;

    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: 0.15 * totalWidth,
            child: Container(
              padding: EdgeInsets.all(padding),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Builder(
                    builder: (context) {
                      return Text(
                        "Jam ke ",
                        textAlign: TextAlign.center,
                      );
                    },
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            width: 0.25 * totalWidth,
            child: Container(
              padding: EdgeInsets.all(padding),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[Text("Waktu")],
              ),
            ),
          ),
          SizedBox(
            width: 0.30 * totalWidth,
            child: Container(
              padding: EdgeInsets.all(padding),
              child: Center(
                child: Text("Mapel"),
              ),
            ),
          ),
          SizedBox(
            width: 0.30 * totalWidth,
            child: Padding(
              padding: EdgeInsets.all(padding),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[Text("Kelas")],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class ContentRow extends StatelessWidget {
  KBM kbmData;
  ContentRow({this.kbmData});
  String getDefinition() {
    var def = kbmData.hour;
//    if(kbmData.schedule == null ) return "";
    String str = "";
    switch (kbmData.day) {
      case "senin":
        str = def.senin;
        break;
      case "selasa":
        str = def.selasa;
        break;
      case "rabu":
        str = def.rabu;
        break;
      case "kamis":
        str = def.kamis;
        break;
      case "jumat":
        str = def.jumat;
        break;
      default:
        str = "n/a";
    }

    return str;
  }

  @override
  Widget build(BuildContext context) {
    double totalWidth = MediaQuery.of(context).size.width;
    var padding = 8.0;
    var time = configs.createDateTime(getDefinition());
    var highlight = false;
    if (time != null) {
      var now = DateTime.now();
      if (now.millisecondsSinceEpoch >= time[0].millisecondsSinceEpoch &&
          now.millisecondsSinceEpoch <= time[1].millisecondsSinceEpoch) {
        highlight = true;
      }
    }
    // TODO: implement build
    return Container(
      color: highlight ? Colors.blue[600] : Colors.white,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: 0.13 * totalWidth,
            child: Container(
              padding: EdgeInsets.all(padding),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Builder(
                    builder: (context) {
                      return Text(kbmData.hour.hour.toString(),
                          style: TextStyle(
                              color: highlight ? Colors.white : Colors.black,
                              fontWeight: highlight
                                  ? FontWeight.bold
                                  : FontWeight.normal));
                    },
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            width: 0.27 * totalWidth,
            child: Container(
              padding: EdgeInsets.all(padding),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Builder(
                    builder: (context) {
                      return Text(getDefinition(),
                          style: TextStyle(
                              color: highlight ? Colors.white : Colors.black,
                              fontWeight: highlight
                                  ? FontWeight.bold
                                  : FontWeight.normal));
                    },
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            width: 0.30 * totalWidth,
            child: Padding(
              padding: EdgeInsets.all(padding),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Builder(builder: (c) {
                    if (kbmData.schedule == null) {
                      return Text("-",
                          style: TextStyle(
                              color: highlight ? Colors.white : Colors.black,
                              fontWeight: highlight
                                  ? FontWeight.bold
                                  : FontWeight.normal));
                    } else
                      return Text(
                        kbmData.schedule.schedule,
                        style: TextStyle(
                            color: highlight ? Colors.white : Colors.black,
                            fontWeight: highlight
                                ? FontWeight.bold
                                : FontWeight.normal),
                      );
                  })
                ],
              ),
            ),
          ),
          SizedBox(
            width: 0.30 * totalWidth,
            child: Padding(
              padding: EdgeInsets.all(padding),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Builder(builder: (c) {
                    if (kbmData.schedule == null) {
                      return Text("-",
                          style: TextStyle(
                              color: highlight ? Colors.white : Colors.black,
                              fontWeight: highlight
                                  ? FontWeight.bold
                                  : FontWeight.normal));
                    } else
                      return Text(kbmData.schedule.assignedClass,
                          style: TextStyle(
                              color: highlight ? Colors.white : Colors.black,
                              fontWeight: highlight
                                  ? FontWeight.bold
                                  : FontWeight.normal));
                  })
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
