import 'package:flutter/material.dart';
import 'package:absensi_mobile/models/data.dart';
import 'package:flutter/services.dart';
import 'package:objectdb/objectdb.dart';
import 'package:absensi_mobile/configs.dart' as configs;
import 'package:moment/moment.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:absensi_mobile/jadwalkbm.dart';
import 'package:intl/intl.dart';
import 'dart:convert';
import 'package:absensi_mobile/loginscreen.dart';
import 'package:absensi_mobile/logabsensi.dart';
import 'package:absensi_mobile/profile.dart';
import 'package:absensi_mobile/profileinfo.dart';
import 'package:sprintf/sprintf.dart';
import 'package:absensi_mobile/adminpage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'createpermitrequest.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  String token;
  ObjectDB db;
  bool loadingUserData = false;
  User mUserData;
  Moment now = Moment();
  Timer timer;
  Attendance todayAttendance;
  bool loadError = false;
  void logout() async {
    await db.open();
    var q = await this.db.remove({"type": "logindata"});
    await db.close();

    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => Login()),
        ModalRoute.withName("./"));
  }

  void onProgress() {
    showDialog(
        builder: (c) {
          return AlertDialog(
            content: Row(
              children: <Widget>[
                Container(
                  child: Icon(
                    Icons.widgets,
                    color: Colors.blue,
                    size: 44,
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: Text("Fitur ini masih dalam masa pengembangan."),
                )
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("Tutup"),
                onPressed: () {
                  Navigator.pop(c);
                },
              )
            ],
          );
        },
        context: this.context);
  }

  @override
  void deactivate() {}

  @override
  void dispose() {
    // TODO: implement dispose
    if (this.timer != null) {
      this.timer.cancel();
    }
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    configs.getDBPath().then((val) async {
      this.db = ObjectDB(val);
      // TODO: implement initState
      checkUserData();
      this.timer = Timer.periodic(Duration(seconds: 1), (timer) {
        setState(() {
          now = Moment();
        });
      });
      MethodChannel(configs.CHANNEL).setMethodCallHandler((method) async {
        if (method.method == "currentSchedule") {
          var hour = method.arguments['hour'] as int;
          debugPrint("HOURRR : $hour");
          await db.open();
          var res = await db.update({'type': 'hourinfo'}, {'hour': hour}, true);
          await db.close();
          if (res < 1) {
            await db.open();
            await db.insert({'type': 'hourinfo', 'hour': hour});
            await db.close();
          }
        }
      });
    });
  }

  void fetchTodayAttendance() async {
    var now = Moment().format("yyyy-MM-dd");
    try {
      var url =
          "${configs.serverUrl}/api/attendance/${mUserData.id.toString()}/data?date=$now";
      var resp = await http.get(url);
      if (resp.statusCode == 200) {
        dynamic data = json.decode(resp.body);
        Attendance attData = Attendance.fromJson(data);
        setState(() {
          todayAttendance = attData;
        });
      }
    } catch (e) {
      print(e);
    }
  }

  void fetchUserInformation(String userid) async {
    setState(() {
      loadError = false;
    });
    try {
      setState(() {
        loadingUserData = true;
      });
      var url = "${configs.serverUrl}/api/cdata/$userid";
      final resp = await http.get(url).timeout(const Duration(seconds: 30));
      if (resp.statusCode == 200) {
        dynamic data = json.decode(resp.body);
        User user = User.fromJson(data);
        setState(() {
          mUserData = user;
        });
        fetchTodayAttendance();

        dynamic hourDefinitions;
        var hourResp =
            await http.get("${configs.serverUrl}/api/hourdefinitions");
        if (hourResp.statusCode == 200) {
          hourDefinitions = json.decode(hourResp.body);

          await db.open();
          db.insert({'type': 'hourdefinition', 'data': hourDefinitions});
          await db.close();
        } else {
          hourDefinitions = [];
        }

        var platform = const MethodChannel('aswad.io/primary');
        platform.invokeMethod('startClockWatcher', {
          'schedule': json.encode(data),
          'hours': json.encode(hourDefinitions)
        });
      } else {
        showDialog(
            builder: (c) {
              AlertDialog(
                content: Text("Cannot get user information!"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Close"),
                    onPressed: () {
                      Navigator.pop(c);
                    },
                  ),
                  FlatButton(
                    child: Text("Retry"),
                    onPressed: () {
                      fetchUserInformation(userid);
                    },
                  )
                ],
              );
            },
            context: this.context);
      }
    } catch (e) {
      setState(() {
        loadError = true;
      });
      print(e);
    } finally {
      setState(() {
        loadingUserData = false;
      });
    }
  }

  void checkUserData() async {
    await db.open();
    List<Map> dbdata = await db.find({'type': 'logindata'});
    final adminData = dbdata.first['logindata'];
    String token = dbdata.first['accesstoken'];
    setState(() {
      this.token = token;
    });
    if (adminData['user'] != null) {
      User tempUData = User.fromJson(adminData['user']);
      if (adminData['logintype'] == 'user') {
        fetchUserInformation(tempUData.id.toString());
      }
    }

    await db.close();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              showDialog(
                  builder: (context) {
                    return AlertDialog(
                      title: Text("Anda akan logout"),
                      content: Text("Lanjutkan?"),
                      actions: <Widget>[
                        FlatButton(
                          onPressed: () {
                            logout();
                          },
                          child: Text("Ya"),
                        ),
                        FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text("Tidak"),
                        )
                      ],
                    );
                  },
                  context: context);
            },
          ),
          IconButton(
            icon: Builder(builder: (c) {
              if (this.loadingUserData) {
                return CircularProgressIndicator(
                  backgroundColor: Colors.white,
                );
              } else
                return Icon(Icons.refresh);
            }),
            onPressed: () {
              checkUserData();
            },
          )
        ],
        backgroundColor: Colors.blueGrey[600],
        title: Text("Aswad Mobile v1.0"),
      ),
      body: Builder(
        builder: (context) {
          if (loadError) {
            return Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Connection Error",
                    style: TextStyle(color: Colors.white),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: RaisedButton(
                      onPressed: () {
                        checkUserData();
                      },
                      child: Text("Retry"),
                    ),
                  )
                ],
              ),
            );
          }
          if (loadingUserData) {
            return Center(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Loading...",
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            return widgetBody();
          }
        },
      ),
      backgroundColor: Colors.blueGrey[900],
    );
  }

  Widget widgetBody() {
    return Column(
      children: <Widget>[
        Container(
            decoration: BoxDecoration(color: Colors.blueGrey[800]),
            padding: EdgeInsets.all(12),
            child: biodata()),
        Container(
          margin: const EdgeInsets.fromLTRB(0, 8, 0, 8),
          child: Center(
            child: Text(
              now.format("d MMM yyyy HH:mm:ss"),
              style: TextStyle(color: Colors.white, fontSize: 15.0),
            ),
          ),
        ),
        EntryExit(
          todayAttendance: this.todayAttendance,
          user: this.mUserData,
        ),
        // Expanded(
        //     child: Container(
        //   margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
        //   child: GridView.count(
        //     crossAxisSpacing: 0.0,
        //     mainAxisSpacing: 0,
        //     crossAxisCount: 3,
        //     children: <Widget>[
        //       MenuItem(() {}, "Log Absensi Anda", Icons.history),
        //       MenuItem(() {}, "Pengajuan Izin", Icons.insert_invitation),
        //       MenuItem(() {}, "Peraturan Jam Masuk", Icons.transit_enterexit),
        //       MenuItem(() {}, "Pencapaian Absensi", Icons.insert_chart),
        //     ],
        //   ),
        // )),
        Expanded(
          child: Container(
            margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
            child: CustomScrollView(
              slivers: <Widget>[
                SliverPadding(
                  padding: const EdgeInsets.all(20.0),
                  sliver: SliverGrid.count(
                    childAspectRatio: 1.2,
                    crossAxisCount: 3,
                    crossAxisSpacing: 0,
                    children: <Widget>[
                      MenuItem(() {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LogAbsensi()));
                      }, "Log Absensi Anda", Icons.history),
                      MenuItem(() {
                        // onProgress();
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (c) => CreatePermitRequest()));
                      }, "Pengajuan Izin", Icons.insert_invitation),
                      MenuItem(() {
                        onProgress();
                      }, "Informasi Jam Masuk", Icons.transit_enterexit),
                      MenuItem(() {
                        onProgress();
                      }, "Pencapaian Absensi", Icons.insert_chart),
                      MenuItem(() {
                        //checkUserData();
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProfileInfo(
                                      userData: this.mUserData,
                                    )));
                      }, "Profil", Icons.person),
                      MenuItem(() {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProfilePage()));
                      }, "Edit Biodata", Icons.edit),
                      MenuItem(() {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => JadwalKBM()));
                      }, "Jadwal KBM", Icons.schedule),
                      Builder(
                        builder: (c) {
                          if (mUserData.admin != null) {
                            return MenuItem(() {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AdminPage(
                                            adminData: mUserData.admin,
                                            token: this.token,
                                          )));
                            }, "Administrasi", Icons.contacts);
                          } else
                            return Container();
                        },
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget biodata() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(4.0),
                child: Text(
                  mUserData?.firstname ?? '-',
                  style: const TextStyle(color: Colors.white, fontSize: 19.0),
                ),
              ),
              Container(
                margin: EdgeInsets.all(4.0),
                child: Text(
                    mUserData?.division != null
                        ? mUserData.division.division
                        : "-",
                    style: TextStyle(color: Colors.white, fontSize: 15.0)),
              ),
              Container(
                margin: EdgeInsets.all(4.0),
                child: Text(
                    mUserData.employmentStatus != null
                        ? mUserData.employmentStatus.employmentStatus
                        : "-",
                    style: TextStyle(color: Colors.white, fontSize: 15.0)),
              ),
              // Container(
              //   margin: EdgeInsets.all(4.0),
              //   child: Text("Login Terakhir Hari ini",
              //       style: TextStyle(color: Colors.white, fontSize: 14.0)),
              // )
            ],
          ),
        ),
        SizedBox(
          child: Container(
            margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
            child: CachedNetworkImage(
              height: 120,
              imageUrl: configs.getPhotoUrl(mUserData.id),
              errorWidget: (c, url, err) =>
                  Image(image: AssetImage("images/smk.png")),
              placeholder: (c, url) => CircularProgressIndicator(),
            ),
          ),
        )
      ],
    );
  }
}

class EntryExit extends StatelessWidget {
  final Attendance todayAttendance;
  final User user;
  bool entryLate = false;
  bool exitEarly = false;
  EntryExit({this.todayAttendance, this.user}) {
    calculateIsLate();
  }
  void calculateIsLate() {
    if (todayAttendance == null) return;
    DateTime entry;
    DateTime exit;
    if (user.personalSchedule != null) {
      var schedule = user.personalSchedule;
      var day = DateTime.now().weekday;
      switch (day) {
        case DateTime.monday:
          {
            if (schedule.seninCustom == 1) {
              entry = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [schedule.seninMasukJam, schedule.seninMasukMenit]));
              exit = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [schedule.seninKeluarJam, schedule.seninKeluarMenit]));
            } else {
              entry = configs.createDateTimeSingle(sprintf("%02d:%02d", [
                user.division.shiftStartHour,
                user.division.shiftStartMinute
              ]));
              exit = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [user.division.shiftEndHour, user.division.shiftEndMinute]));
            }
          }
          break;
        case DateTime.tuesday:
          {
            if (schedule.selasaCustom == 1) {
              entry = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [schedule.selasaMasukJam, schedule.selasaMasukMenit]));
              exit = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [schedule.rabuKeluarJam, schedule.rabuKeluarMenit]));
            } else {
              entry = configs.createDateTimeSingle(sprintf("%02d:%02d", [
                user.division.shiftStartHour,
                user.division.shiftStartMinute
              ]));
              exit = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [user.division.shiftEndHour, user.division.shiftEndMinute]));
            }
          }
          break;
        case DateTime.wednesday:
          {
            if (schedule.rabuCustom == 1) {
              entry = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [schedule.rabuMasukJam, schedule.rabuMasukMenit]));
              exit = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [schedule.rabuKeluarJam, schedule.rabuKeluarMenit]));
            } else {
              entry = configs.createDateTimeSingle(sprintf("%02d:%02d", [
                user.division.shiftStartHour,
                user.division.shiftStartMinute
              ]));
              exit = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [user.division.shiftEndHour, user.division.shiftEndMinute]));
            }
          }
          break;
        case DateTime.thursday:
          {
            if (schedule.kamisCustom == 1) {
              entry = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [schedule.kamisMasukJam, schedule.kamisMasukMenit]));
              exit = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [schedule.kamisKeluarJam, schedule.kamisKeluarMenit]));
            } else {
              entry = configs.createDateTimeSingle(sprintf("%02d:%02d", [
                user.division.shiftStartHour,
                user.division.shiftStartMinute
              ]));
              exit = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [user.division.shiftEndHour, user.division.shiftEndMinute]));
            }
          }
          break;
        case DateTime.friday:
          {
            if (schedule.jumatCustom == 1) {
              entry = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [schedule.jumatMasukJam, schedule.jumatMasukMenit]));
              exit = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [schedule.jumatKeluarJam, schedule.jumatKeluarMenit]));
            } else {
              entry = configs.createDateTimeSingle(sprintf("%02d:%02d", [
                user.division.shiftStartHour,
                user.division.shiftStartMinute
              ]));
              exit = configs.createDateTimeSingle(sprintf("%02d:%02d",
                  [user.division.shiftEndHour, user.division.shiftEndMinute]));
            }
          }
          break;
      }
    } else {
      entry = configs.createDateTimeSingle(sprintf("%02d:%02d",
          [user.division.shiftStartHour, user.division.shiftStartMinute]));
      exit = configs.createDateTimeSingle(sprintf("%02d:%02d",
          [user.division.shiftEndHour, user.division.shiftEndMinute]));
    }

    var attEntry = configs.createDateTimeSingle(sprintf("%02d:%02d",
        [todayAttendance.entryDate.hour, todayAttendance.entryDate.minute]));
    if (attEntry.millisecondsSinceEpoch > entry.millisecondsSinceEpoch)
      this.entryLate = true;

    if (todayAttendance.exitDate != null) {
      var attExit = configs.createDateTimeSingle(sprintf("%02d:%02d",
          [todayAttendance.exitDate.hour, todayAttendance.exitDate.minute]));
      this.exitEarly =
          attExit.millisecondsSinceEpoch < exit.millisecondsSinceEpoch;
    }
//    if(at.millisecondsSinceEpoch > entry.millisecondsSinceEpoch) this.entryLate = true;
  }

  @override
  Widget build(BuildContext context) {
    calculateIsLate();
    // TODO: implement build
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: Colors.white),
          borderRadius: BorderRadius.all(Radius.circular(3))),
      margin: EdgeInsets.fromLTRB(8, 4, 8, 4),
      child: Container(
        padding: EdgeInsets.all(8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: Text(
                    "Wangsul",
                    style: TextStyle(color: Colors.white, fontSize: 17.0),
                  ),
                ),
                Builder(builder: (c) {
                  if (todayAttendance != null) {
                    return Text(
                      DateFormat("HH:mm").format(todayAttendance.entryDate),
                      style: TextStyle(
                          color: entryLate ? Colors.red : Colors.green,
                          fontSize: 25.0),
                    );
                  } else {
                    return Text(
                      "Belum Masuk",
                      style: TextStyle(color: Colors.amber, fontSize: 25.0),
                    );
                  }
                })
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: Text(
                    "Damel",
                    style: TextStyle(color: Colors.white, fontSize: 17.0),
                  ),
                ),
                Builder(builder: (c) {
                  if (todayAttendance != null) {
                    if (todayAttendance.exitDate != null) {
                      return Text(
                        DateFormat("HH:mm").format(todayAttendance.exitDate),
                        style: TextStyle(
                            color: exitEarly ? Colors.red : Colors.green,
                            fontSize: 25.0),
                      );
                    } else {
                      return Text(
                        "Belum Keluar",
                        style: TextStyle(color: Colors.green, fontSize: 25.0),
                      );
                    }
                  } else {
                    return Text(
                      "Belum Masuk",
                      style: TextStyle(color: Colors.amber, fontSize: 25.0),
                    );
                  }
                })
              ],
            )
          ],
        ),
      ),
    );
  }
}

class MenuItem extends StatelessWidget {
  Function onClick;
  String title;
  IconData icon;
  MenuItem(Function onClick, String title, IconData icon) {
    this.onClick = onClick;
    this.title = title;
    this.icon = icon;
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: this.onClick,
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              this.icon,
              size: 34,
              color: Colors.white,
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
              child: Text(
                title,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ButtonWidget extends StatelessWidget {
  Function onClick;
  String title;
  IconData icon;
  ButtonWidget(Function onClick, String title, IconData icon) {
    this.onClick = onClick;
    this.title = title;
    this.icon = icon;
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: this.onClick,
      child: Column(
        children: <Widget>[
          Icon(
            this.icon,
            size: 34,
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
            child: Text(
              title,
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}
