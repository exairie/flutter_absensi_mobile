import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:absensi_mobile/configs.dart';
import 'package:absensi_mobile/models/data.dart';
import 'package:objectdb/objectdb.dart';
import 'dart:core';
import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:path/path.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

class CreatePermitRequest extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return CreatePermitRequestState();
  }
}

class CreatePermitRequestState extends State<CreatePermitRequest> {
  ObjectDB db;
  User userData;
  File image;
  DateTime requestFrom = DateTime.now(), requestTo = DateTime.now();
  int permitType = 0;
  var permits = ['izin', 'izin_lain', 'sakit', 'cuti', 'cuti_lain'];
  //TextEditingControllers
  TextEditingController controllerReason = TextEditingController();
  //

  String getPermitType() => permits[permitType];
  void created() async {
    await transaction(db, (db) async {
      List<Map> dbdata = await db.find({'type': 'logindata'});
      final adminData = dbdata.first['logindata'];
      if (adminData['user'] != null) {
        User tempUData = User.fromJson(adminData['user']);
        setState(() {
          this.userData = tempUData;
        });
        // loadSchedules();
      }
      return Future.value();
    });
  }

  void save({bool onlyPicture = false}) async {
    /** Send Data first */
    var format = DateFormat("yyyy-MM-dd");
    int permitRequestId = -1;
    if (!onlyPicture) {
      var resp = await http.post("$serverUrl/api/permitrequest/data", body: {
        'enrollment_id': userData.id.toString(),
        'permit_from': format.format(requestFrom),
        'permit_to': format.format(requestTo.add(Duration(days: 1))),
        'reason': controllerReason.text,
        'permit_type': getPermitType(),
      });

      if (resp.statusCode != 200) {
        showDialog(
            builder: (c) => AlertDialog(
                  content: Text("Communication failed. Retry?"),
                  actions: <Widget>[
                    FlatButton(
                      child: Text("Close"),
                      onPressed: () {
                        Navigator.pop(c);
                      },
                    ),
                    FlatButton(
                      child: Text("Retry"),
                      onPressed: () {
                        save();
                        Navigator.pop(c);
                      },
                    ),
                  ],
                ),
            context: this.context);
      } else {
        dynamic data = json.decode(resp.body);
        permitRequestId = data['id'];
      }
    }
    var uploadfile = http.MultipartFile.fromBytes(
        'img', await image.readAsBytes(),
        filename: 'upload.jpeg');
    var request = http.MultipartRequest('POST',
        Uri.parse("${serverUrl}/api/permitrequest/${permitRequestId}/image"));

    request.files.add(uploadfile);

    var resp = await request.send();
    if (resp.statusCode == 200) {
      showDialog(
          builder: (c) => AlertDialog(
                content: Text(
                    "Data telah disimpan!\nPengajuan izin harus di acc terlebih dahulu sebelum anda tercatat izin"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Close"),
                    onPressed: () {
                      Navigator.pop(c);
                      Navigator.pop(c);
                    },
                  ),
                ],
              ),
          context: this.context);
    } else {
      showDialog(
          builder: (c) => AlertDialog(
                content: Text("Communication failed. Retry?"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Close"),
                    onPressed: () {
                      Navigator.pop(c);
                    },
                  ),
                  FlatButton(
                    child: Text("Retry"),
                    onPressed: () {
                      save(onlyPicture: true);
                      Navigator.pop(c);
                    },
                  ),
                ],
              ),
          context: this.context);
    }
  }

  void pickImage() async {
    var img = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      this.image = img;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDBPath().then((path) {
      this.db = ObjectDB(path);
      created();
    });
  }

  Widget header() {
    if (userData == null)
      return Container(
        padding: EdgeInsets.all(40),
        child: Center(
          child: Row(
            children: <Widget>[
              SizedBox(
                width: 30,
                height: 30,
                child: CircularProgressIndicator(),
              ),
              Padding(
                  child: Text("Loading..."), padding: EdgeInsets.only(left: 8)),
            ],
          ),
        ),
      );
    return Row(
      children: <Widget>[
        Expanded(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(8),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: 8),
                      child: Icon(
                        Icons.insert_invitation,
                        size: 24,
                        color: Colors.grey[700],
                      ),
                    ),
                    Text(
                      "Ajukan Izin",
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey[700]),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 8, top: 8, bottom: 2),
                child: Text(
                  "Atas Nama",
                  style: TextStyle(fontSize: 13),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 8, bottom: 2),
                child: Text(
                  userData?.firstname ?? "-",
                  style: TextStyle(fontSize: 19),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 8),
                child: Text(
                  "${(userData?.division?.division ?? "-")}/${(userData?.employmentStatus?.employmentStatus ?? "-")}",
                  style: TextStyle(fontSize: 16),
                ),
              )
            ],
            crossAxisAlignment: CrossAxisAlignment.start,
          ),
        ),
        CachedNetworkImage(
          width: 120,
          imageUrl: getPhotoUrl(userData?.id),
          errorWidget: (c, url, err) => Image(
                image: AssetImage("images/smk.png"),
                width: 120,
              ),
          placeholder: (c, url) => CircularProgressIndicator(),
        )
      ],
    );
  }

  void pickDateTo() async {
    final DateTime picker = await showDatePicker(
        context: this.context,
        initialDate: requestFrom,
        firstDate: DateTime(DateTime.now().year - 2),
        lastDate: DateTime(DateTime.now().year + 1));
    if (picker == null) return;
    setState(() {
      this.requestTo = picker;
    });
  }

  void pickDateFrom() async {
    final DateTime picker = await showDatePicker(
        context: this.context,
        initialDate: requestFrom,
        firstDate: DateTime(DateTime.now().year - 2),
        lastDate: DateTime(DateTime.now().year + 1));
    if (picker == null) return;
    setState(() {
      this.requestFrom = picker;
    });
  }

  bool validate() {
    if (requestTo.millisecondsSinceEpoch > requestFrom.millisecondsSinceEpoch)
      return false;
    if (controllerReason.text.length < 5) return false;
    if (image == null) return false;
    return true;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          Builder(
            builder: (c) {
              if (validate()) {
                return FlatButton(
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.save,
                        color: Colors.white,
                      ),
                      Text(
                        "Simpan",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                  onPressed: save,
                );
              } else {
                return FlatButton(
                  onPressed: () {},
                  child: Text(
                    "Silahkan lengkapi data",
                    style: TextStyle(color: Colors.white),
                  ),
                );
              }
            },
          )
        ],
        title: Text("Ajukan Izin"),
      ),
      body: ListView(
        children: <Widget>[
          Card(
            margin: EdgeInsets.all(4),
            child: Container(
              child: header(),
              padding: EdgeInsets.all(8),
            ),
          ),
          Card(
            margin: EdgeInsets.all(4),
            child: Container(
              padding: EdgeInsets.all(8),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  BlockText("Informasi Izin", Colors.blueAccent),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Radio(
                            onChanged: (d) {
                              setState(() {
                                permitType = d;
                              });
                            },
                            value: 0,
                            groupValue: permitType,
                          ),
                          Text("Dinas Luar")
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Radio(
                            onChanged: (d) {
                              setState(() {
                                permitType = d;
                              });
                            },
                            value: 1,
                            groupValue: permitType,
                          ),
                          Text("Izin Pribadi")
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Radio(
                            onChanged: (d) {
                              setState(() {
                                permitType = d;
                              });
                            },
                            value: 2,
                            groupValue: permitType,
                          ),
                          Text("Sakit")
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Radio(
                            onChanged: (d) {
                              setState(() {
                                permitType = d;
                              });
                            },
                            value: 3,
                            groupValue: permitType,
                          ),
                          Text("Cuti (SK)")
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Radio(
                            onChanged: (d) {
                              setState(() {
                                permitType = d;
                              });
                            },
                            value: 4,
                            groupValue: permitType,
                          ),
                          Text("Cuti Lain")
                        ],
                      ),
                    ],
                  ),
                  Divider(
                    color: Colors.grey[600],
                    height: 40,
                  ),
                  TextFormField(
                    validator: (s) {
                      return s.length > 5
                          ? null
                          : "Alasan izin harus lebih dari 5 huruf";
                    },
                    autovalidate: true,
                    decoration: InputDecoration(
                      labelText: "Alasan Izin",
                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(4.0),
                        borderSide: new BorderSide(),
                      ),
                    ),
                    controller: controllerReason,
                  ),
                  Divider(
                    color: Colors.grey[600],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Text(
                      "Lama Izin",
                      style: TextStyle(fontSize: 19),
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(top: 8),
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Text("dari"),
                                Container(
                                  margin: EdgeInsets.only(top: 5),
                                  child: GestureDetector(
                                      onTap: () {
                                        pickDateFrom();
                                      },
                                      child: Text(
                                        DateFormat('dd MMM yyyy')
                                            .format(requestFrom),
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 23),
                                      )),
                                  padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                                  decoration: BoxDecoration(
                                      color: Colors.amber,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(4))),
                                ),
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Text("sampai"),
                                Container(
                                  margin: EdgeInsets.only(top: 5),
                                  child: GestureDetector(
                                    child: Text(
                                      DateFormat('dd MMM yyyy')
                                          .format(requestTo),
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 23),
                                    ),
                                    onTap: () {
                                      pickDateTo();
                                    },
                                  ),
                                  padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                                  decoration: BoxDecoration(
                                      color: Colors.amber,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(4))),
                                ),
                              ],
                            )
                          ]))
                ],
              ),
            ),
          ),
          Card(
              margin: const EdgeInsets.all(4),
              child: Container(
                padding: const EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    BlockText("Bukti (Foto surat sakit/bukti izin lain)",
                        Colors.blue),
                    Builder(
                      builder: (c) {
                        if (this.image != null)
                          return Container(
                              margin: const EdgeInsets.only(top: 8),
                              child: Image.file(image));
                        else
                          return Container(
                              margin: const EdgeInsets.only(top: 10),
                              color: Colors.green,
                              padding: EdgeInsets.all(10),
                              child: GestureDetector(
                                onTap: () {
                                  pickImage();
                                },
                                child: Center(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(right: 10),
                                      child: Icon(
                                        Icons.camera_alt,
                                        color: Colors.white,
                                      ),
                                    ),
                                    Text(
                                      "Pilih gambar",
                                      style: TextStyle(color: Colors.white),
                                    )
                                  ],
                                )),
                              ));
                      },
                    )
                  ],
                ),
              ))
        ],
      ),
    );
  }
}

class BlockText extends StatelessWidget {
  final String text;
  final Color color;

  BlockText(this.text, this.color);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Text(
        text,
        style: TextStyle(fontSize: 16),
      ),
      padding: EdgeInsets.only(left: 6, top: 4, bottom: 4),
      decoration: BoxDecoration(
          border: Border(left: BorderSide(width: 4, color: this.color))),
    );
  }
}
