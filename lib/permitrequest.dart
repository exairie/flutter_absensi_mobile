import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:absensi_mobile/configs.dart' as configs;
import 'package:absensi_mobile/models/data.dart';
import 'package:objectdb/objectdb.dart';
import 'package:cached_network_image/cached_network_image.dart';

class PermitRequest extends StatefulWidget {
  AdminData adminData;
  String token;
  PermitRequest({this.adminData, this.token});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PermitRequestState();
  }
}

class PermitRequestState extends State<PermitRequest> {
  List<PermitRequestData> data = List<PermitRequestData>();
  AdminData adminData;
  bool loading = false;
  ObjectDB db;
  String token;
  PermitRequestState();
  Future created() async {
    await configs.transaction(this.db, (db) async {
      var completer = Completer();
      List<Map> dbdata = await db.find({'type': 'logindata'});
      final adminData = dbdata.first['logindata'];
      var token = dbdata.first['token'];
      if (adminData['admin'] != null) {
        AdminData tempUData = AdminData.fromJson(adminData['admin']);
        setState(() {
          this.adminData = tempUData;
          this.token = token;
        });
      }
      completer.complete();
      return completer.future;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    adminData = widget.adminData;
    token = widget.token;
    configs.getDBPath().then((path) async {
      db = ObjectDB(path);
      if (adminData == null || token == null) await created();
      loadData();
    });
  }

  void loadData({String showOnly}) async {
    setState(() {
      loading = true;
    });
    try {
      var resp = await http.get(
          "${configs.serverUrl}/permitapi/${showOnly == null ? "" : "?status=" + showOnly}",
          headers: {'token': this.token});
      if (resp.statusCode == 200) {
        if (resp.statusCode == 200) {
          dynamic data = json.decode(resp.body);
          List<PermitRequestData> permitData = List<PermitRequestData>();
          for (var i in data) {
            permitData.add(PermitRequestData.fromJson(i));
          }

          setState(() {
            this.data = permitData;
          });
        }
      }
    } catch (e) {
      print(e);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
        initialIndex: 0,
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: <Widget>[
                Tab(
                  text: "Permohonan",
                ),
                Tab(
                  text: "Diizinkan",
                ),
                Tab(
                  text: "Ditolak",
                )
              ],
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.refresh),
                onPressed: () {},
              )
            ],
            title: Text("Permohonan izin"),
          ),
          body: Builder(
            builder: (c) {
              if (loading)
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircularProgressIndicator(),
                      Container(
                        margin: EdgeInsets.all(10),
                        child: Text("Loading..."),
                      )
                    ],
                  ),
                );
              else
                return TabBarView(
                  children: <Widget>[
                    widgetBody(filter: "requested"),
                    widgetBody(filter: "accepted"),
                    widgetBody(filter: "rejected")
                  ],
                );
            },
          ),
        ));
  }

  void acceptOk() {
    showDialog(
        context: context,
        builder: (c) {
          return AlertDialog(
            title: Text("Perizinan"),
            content: Text("Data disimpan!"),
            actions: <Widget>[
              FlatButton(
                child: Text("Tutup"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  void allowPermit(PermitRequestData p) async {
    try {
      var resp = await http
          .put("${configs.serverUrl}/permitapi/${p.id}/apply", body: {});
      if (resp.statusCode == 200) {
        acceptOk();
        loadData();
      }
    } catch (e) {
      print(e);
    }
  }

  void denyPermit(PermitRequestData p) async {
    try {
      var resp = await http.delete("${configs.serverUrl}/permitapi/${p.id}");
      if (resp.statusCode == 200) {
        acceptOk();
        loadData();
      }
    } catch (e) {
      print(e);
    }
  }

  void applyPermit(PermitRequestData p) {
    showDialog(
        context: this.context,
        builder: (c) {
          return AlertDialog(
            title: Text("Terima/Tolak Izin"),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Silahkan tindaklanjuti perihal izin berikut",
                  style: TextStyle(color: Colors.grey[900]),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: Text(
                    p.enrollment.firstname,
                    style: TextStyle(color: Colors.grey[800], fontSize: 15),
                  ),
                ),
                Text(
                  p.reason,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                )
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("Izinkan"),
                onPressed: () {
                  allowPermit(p);
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                child: Text("Tolak Izin"),
                onPressed: () {
                  denyPermit(p);
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                child: Text("Tutup"),
                onPressed: () => Navigator.pop(context),
              )
            ],
          );
        });
  }

  Widget widgetBody({String filter}) {
    var data = this.data.where((p) {
      return p.status == filter;
    }).toList();
    return Container(
      child: Builder(
        builder: (c) {
          if (data.length > 0) {
            return ListView.builder(
              itemCount: data.length,
              itemBuilder: (c, i) {
                return PermitRow(data[i], () {
                  applyPermit(data[i]);
                });
              },
            );
          } else
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.info,
                    size: 80,
                    color: Colors.blue,
                  ),
                  Text(
                    "No Data",
                    style: TextStyle(fontSize: 19, color: Colors.blue),
                  )
                ],
              ),
            );
        },
      ),
    );
  }
}

class PermitRow extends StatelessWidget {
  Function onSelect;
  PermitRequestData data;
  PermitRow(this.data, this.onSelect);
  String dateFrom() {
    var d1 = DateTime.parse(data.permitFrom).toLocal();
    var d2 = DateTime.parse(data.permitTo).toLocal();

    var from = DateFormat("dd MMM yyyy").format(d1);
    var to = DateFormat("dd MMM yyyy").format(d2);

    return "$from - $to";
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        this.onSelect();
      },
      child: Container(
        margin: EdgeInsets.all(4),
        child: Card(
          child: Container(
            padding: EdgeInsets.all(8),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  width: 130,
                  child: Center(
                    child: CachedNetworkImage(
                      height: 110,
                      imageUrl: configs.getPhotoUrl(data.enrollment.id),
                      errorWidget: (c, url, err) =>
                          Image(image: AssetImage("images/smk.png")),
                      placeholder: (c, url) => CircularProgressIndicator(),
                    ),
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          data.enrollment.firstname,
                          style: TextStyle(fontSize: 22),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(bottom: 8),
                        decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        // color: Colors.blue,
                        padding: EdgeInsets.all(4),
                        child: Text(
                          "Dinas Luar",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      Text(
                        "Pengajuan Izin untuk tanggal",
                        style: TextStyle(fontSize: 12),
                      ),
                      Text(
                        dateFrom(),
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.bold),
                      ),
                      Container(
                          margin: const EdgeInsets.only(top: 8),
                          child: Text(
                            "Alasan izin",
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          )),
                      Text(
                        data.reason,
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.open_in_new),
                  onPressed: () {},
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
