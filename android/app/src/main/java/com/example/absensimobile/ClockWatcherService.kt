package com.example.absensimobile

import android.app.Notification
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Handler
import android.os.IBinder
import com.google.gson.Gson
import java.text.DateFormat
import java.util.*
import android.R.attr.description
import android.R.attr.name
import android.app.NotificationChannel


class ClockWatcherService : Service() {
    data class DeltaSchedule(val schedule : EnrollmentSchedulesItem, var deltaStart : Long, val deltaEnd : Long, val calStart : Calendar, val calEnd : Calendar)
    data class CurrentHour(val definition: HourDefinition, val start : Calendar, val end : Calendar)
    val day = arrayOf("minggu","senin","selasa","rabu","kamis","jumat","sabtu")
    val regex = Regex("[0-9]{2}\\:[0-9]{2}\\s-\\s[0-9]{2}\\:[0-9]{2}")
    var manager : NotificationManager? = null
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notif = Notification.Builder(this)
                .setContentTitle("Aswad Mobile")
                .setContentText("Notification Service Running")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel("aswad_schedule", "Aswad Schedule Notification", NotificationManager.IMPORTANCE_DEFAULT)
            manager!!.createNotificationChannel(channel)
            notif.setChannelId("aswad_schedule")
        }
        manager!!.notify(11,notif.build())
        if(intent == null)return START_NOT_STICKY
        val gson = Gson()
        val enrollmentData = gson.fromJson<Enrollment>(intent.getStringExtra("schedule"), Enrollment::class.java)
        val hours = gson.fromJson<Array<HourDefinition>>(intent.getStringExtra("hours"), Array<HourDefinition>::class.java)

        startReminders(enrollmentData, hours)

        return START_STICKY
    }
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    fun startReminders(enrollmentData : Enrollment, hours : Array<HourDefinition>){
        val mDay = day[Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1]
        if(enrollmentData.enrollmentSchedules == null) return
        val today = enrollmentData.enrollmentSchedules.filter { e->e.day == mDay }
        if(today.count() < 1) return

        val now = Calendar.getInstance()
        val deltas = mutableListOf<DeltaSchedule>()
        val chours = mutableListOf<CurrentHour>()

        hours.forEach {
            val f = getHourDefinition(it)
            val d = getDateTime(f)
            if(d != null){
                chours.add(CurrentHour(it, d[0],d[1]))
            }
        }

        for(i in today){
            if(i.hourDefinition != null){
                val dateArr = getDateTime(getHourDefinition(i.hourDefinition)) ?: continue
                val dstart = Math.abs(now.timeInMillis - dateArr[0].timeInMillis)
                val dend = Math.abs(now.timeInMillis - dateArr[1].timeInMillis)

                deltas.add(DeltaSchedule(i,dstart,dend, dateArr[0],dateArr[1]))
            }
        }

        if(deltas.count() > 0) {
            deltas.sortBy { t -> t.calEnd }
            val shortest = deltas.first()
            if (shortest.calStart.timeInMillis <= 5 * 60 * 1000) {
                if(manager != null) {
                    val notif = Notification.Builder(this)
                            .setContentTitle("Jam mengajar")
                            .setContentText("Jadwal mengajar : ${shortest.schedule.schedule} - ${shortest.schedule.AssignedClass} pada ${android.text.format.DateFormat.format("HH:mm", shortest.calStart)}")
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notif.setChannelId("aswad_schedule")
                    }
                    manager!!.notify(10, notif.build())
                }
            }
        }

        for(i in chours){
            if(now.timeInMillis >= i.start.timeInMillis && now.timeInMillis <= i.end.timeInMillis){
                val intent = Intent(BuildConfig.APPLICATION_ID + ".jobReceiver")
                intent.putExtra("currentSchedule",i.definition.hour)
                sendBroadcast(intent)
                break
            }
        }

        Handler().postDelayed(Runnable { startReminders(enrollmentData, hours) },60 * 1000)
    }

    fun getHourDefinition(hr : HourDefinition):String{
        when(Calendar.getInstance().get(Calendar.DAY_OF_WEEK)){
            Calendar.MONDAY->return hr.senin
            Calendar.TUESDAY->return hr.selasa
            Calendar.WEDNESDAY->return hr.rabu
            Calendar.THURSDAY->return hr.kamis
            Calendar.FRIDAY->return hr.jumat
        }
        return "-"
    }

    fun getDateTime(date : String): Array<Calendar>? {
        if(regex.matches(date)){
            try {
                val dsp = date.split("-").map { e->e.trim() }
                val d1 = dsp[0];val d2 = dsp[1]

                val cal1 = Calendar.getInstance()

                cal1.set(Calendar.HOUR_OF_DAY, d1.substring(0,2).toInt())
                cal1.set(Calendar.MINUTE, d1.substring(3,5).toInt())
                cal1.set(Calendar.SECOND,0)

                val cal2 = Calendar.getInstance()
                cal2.set(Calendar.HOUR_OF_DAY, d2.substring(0,2).toInt())
                cal2.set(Calendar.MINUTE, d2.substring(3,5).toInt())
                cal2.set(Calendar.SECOND,0)
                return arrayOf(cal1,cal2)
            }catch (e : Exception){
                return null
            }
        }

        return null
    }
}
