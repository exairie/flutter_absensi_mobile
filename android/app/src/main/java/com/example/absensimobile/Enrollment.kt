package com.example.absensimobile


import com.google.gson.annotations.SerializedName

data class EnrollmentSchedulesItem(@SerializedName("schedule")
                                   val schedule: String = "",
                                   @SerializedName("hour")
                                   val hour: Int = 0,
                                   @SerializedName("updated_at")
                                   val updatedAt: String = "",
                                   @SerializedName("created_at")
                                   val createdAt: String = "",
                                   @SerializedName("id")
                                   val id: Int = 0,
                                   @SerializedName("hour_definition")
                                   val hourDefinition: HourDefinition?,
                                   @SerializedName("day")
                                   val day: String = "",
                                   @SerializedName("class")
                                   val AssignedClass: String = "",
                                   @SerializedName("enrollment_id")
                                   val enrollmentId: Int = 0)


data class HourDefinition(@SerializedName("kamis")
                          val kamis: String = "",
                          @SerializedName("hour")
                          val hour: Int = 0,
                          @SerializedName("updated_at")
                          val updatedAt: String = "",
                          @SerializedName("senin")
                          val senin: String = "",
                          @SerializedName("rabu")
                          val rabu: String = "",
                          @SerializedName("jumat")
                          val jumat: String = "",
                          @SerializedName("created_at")
                          val createdAt: String = "",
                          @SerializedName("selasa")
                          val selasa: String = "",
                          @SerializedName("id")
                          val id: Int = 0)


data class EmploymentStatus(@SerializedName("updated_at")
                            val updatedAt: String = "",
                            @SerializedName("created_at")
                            val createdAt: String = "",
                            @SerializedName("id")
                            val id: String = "",
                            @SerializedName("employment_status")
                            val employmentStatus: String = "")


data class Enrollment(@SerializedName("firstname")
                      val firstname: String = "",
                      @SerializedName("gender")
                      val gender: String = "",
                      @SerializedName("nuptk")
                      val nuptk: String = "",
                      @SerializedName("created_at")
                      val createdAt: String = "",
                      @SerializedName("enrollment_schedules")
                      val enrollmentSchedules: List<EnrollmentSchedulesItem>?,
                      @SerializedName("photo1")
                      val photo: String = "",
                      @SerializedName("division")
                      val division: Division,
                      @SerializedName("nik")
                      val nik: String = "",
                      @SerializedName("password")
                      val password: String = "",
                      @SerializedName("nip")
                      val nip: String = "",
                      @SerializedName("updated_at")
                      val updatedAt: String = "",
                      @SerializedName("id")
                      val id: Int = 0,
                      @SerializedName("employment_status")
                      val employmentStatus: EmploymentStatus,
                      @SerializedName("email")
                      val email: String = "",
                      @SerializedName("address")
                      val address: String = "",
                      @SerializedName("kota")
                      val kota: String = "",
                      @SerializedName("division_id")
                      val divisionId: String = "",
                      @SerializedName("active")
                      val active: Int = 0,
                      @SerializedName("kelurahan")
                      val kelurahan: String = "",
                      @SerializedName("lastname")
                      val lastname: String = "",
                      @SerializedName("phone")
                      val phone: String = "",
                      @SerializedName("grade")
                      val grade: String = "",
                      @SerializedName("admin_id")
                      val adminId: Int? = null,
                      @SerializedName("kecamatan")
                      val kecamatan: String = "",
                      @SerializedName("duk")
                      val duk: Int = 0,
                      @SerializedName("employment_status_id")
                      val employmentStatusId: String = "",

                      @SerializedName("username")
                      val username: String = "")


data class Division(@SerializedName("shift_end_minute")
                    val shiftEndMinute: Int = 0,
                    @SerializedName("closing_minute")
                    val closingMinute: Int = 0,
                    @SerializedName("estimated_work_hour")
                    val estimatedWorkHour: Int = 0,
                    @SerializedName("break_hour")
                    val breakHour: Int = 0,
                    @SerializedName("created_at")
                    val createdAt: String = "",
                    @SerializedName("division")
                    val division: String = "",
                    @SerializedName("updated_at")
                    val updatedAt: String = "",
                    @SerializedName("closing_hour")
                    val closingHour: Int = 0,
                    @SerializedName("shift_start_hour")
                    val shiftStartHour: Int = 0,
                    @SerializedName("shift_start_minute")
                    val shiftStartMinute: Int = 0,
                    @SerializedName("id")
                    val id: String = "",
                    @SerializedName("shift_end_hour")
                    val shiftEndHour: Int = 0,
                    @SerializedName("break_minute")
                    val breakMinute: Int = 0,
                    @SerializedName("break_hour_end")
                    val breakHourEnd: Int = 0,
                    @SerializedName("break_minute_end")
                    val breakMinuteEnd: Int = 0)


