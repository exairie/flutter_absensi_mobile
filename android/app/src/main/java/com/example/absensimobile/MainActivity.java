package com.example.absensimobile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import io.flutter.app.FlutterActivity;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.view.FlutterMain;

import android.util.Log;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends FlutterActivity {
   private static final String CHANNEL = "aswad.io/primary";
   BroadcastReceiver jobReceiver = new BroadcastReceiver() {
       @Override
       public void onReceive(Context context, Intent intent) {
           Map<String, String> map = new HashMap<>();
           map.put("hour",String.valueOf(intent.getIntExtra("currentSchedule",-1)));
           new MethodChannel(getFlutterView(),CHANNEL).invokeMethod("currentSchedule",map);
       }
   };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
      FlutterMain.startInitialization(this); //Added line
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);

    new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(
            (call, result) -> {
                if(call.method.equals("startClockWatcher")){
                    Toast.makeText(this,"Clock Watcher Start", Toast.LENGTH_LONG).show();
                  //Toast.makeText(MainActivity.this,"Hellooo",Toast.LENGTH_LONG).show();
                    String schedule = call.argument("schedule");
                    String hours = call.argument("hours");
                    Intent i = new Intent(MainActivity.this, ClockWatcherService.class);
                    i.putExtra("schedule",schedule);
                    i.putExtra("hours",hours);
                    startService(i);
                }
            });
    try {
        registerReceiver(jobReceiver, new IntentFilter(BuildConfig.APPLICATION_ID + ".jobReceiver"));
    }catch (Exception e){
        e.printStackTrace();
    }
  }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(jobReceiver);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
